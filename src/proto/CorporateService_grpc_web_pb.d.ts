import * as grpcWeb from 'grpc-web';
import {
  AuthenticateRequest,
  AuthenticateResponse,
  Client,
  ClientAddRequest,
  ClientAddResponse,
  ClientEditRequest,
  ClientEditResponse,
  ClientListGetRequest,
  ClientListGetResponse,
  ClientRemoveRequest,
  ClientRemoveResponse,
  Corporation,
  KeywordRegenerateRequest,
  KeywordRegenerateResponse,
  Order,
  OrderAddRequest,
  OrderAddResponse,
  OrderCancelRequest,
  OrderCancelResponse,
  OrderListGetRequest,
  OrderListGetResponse,
  OrderListSingleGetRequest,
  OrderListSingleGetResponse,
  Point,
  Tariff,
  TariffGroup,
  TariffListGetRequest,
  TariffListGetResponse,
  WhoAmIRequest,
  WhoAmIResponse} from './CorporateService_pb';

export class CorporateServiceClient {
  constructor (hostname: string,
               credentials: null | { [index: string]: string; },
               options: null | { [index: string]: string; });

  authenticate(
    request: AuthenticateRequest,
    metadata: grpcWeb.Metadata,
    callback: (err: grpcWeb.Error,
               response: AuthenticateResponse) => void
  ): grpcWeb.ClientReadableStream<AuthenticateResponse>;

  whoAmI(
    request: WhoAmIRequest,
    metadata: grpcWeb.Metadata,
    callback: (err: grpcWeb.Error,
               response: WhoAmIResponse) => void
  ): grpcWeb.ClientReadableStream<WhoAmIResponse>;

  keywordRegenerate(
    request: KeywordRegenerateRequest,
    metadata: grpcWeb.Metadata,
    callback: (err: grpcWeb.Error,
               response: KeywordRegenerateResponse) => void
  ): grpcWeb.ClientReadableStream<KeywordRegenerateResponse>;

  tariffListGet(
    request: TariffListGetRequest,
    metadata: grpcWeb.Metadata,
    callback: (err: grpcWeb.Error,
               response: TariffListGetResponse) => void
  ): grpcWeb.ClientReadableStream<TariffListGetResponse>;

  orderListGet(
    request: OrderListGetRequest,
    metadata: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<OrderListGetResponse>;

  orderCancel(
    request: OrderCancelRequest,
    metadata: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<OrderCancelResponse>;

  orderListSingleGet(
    request: OrderListSingleGetRequest,
    metadata: grpcWeb.Metadata,
    callback: (err: grpcWeb.Error,
               response: OrderListSingleGetResponse) => void
  ): grpcWeb.ClientReadableStream<OrderListSingleGetResponse>;

  orderAdd(
    request: OrderAddRequest,
    metadata: grpcWeb.Metadata,
    callback: (err: grpcWeb.Error,
               response: OrderAddResponse) => void
  ): grpcWeb.ClientReadableStream<OrderAddResponse>;

  clientListGet(
    request: ClientListGetRequest,
    metadata: grpcWeb.Metadata,
    callback: (err: grpcWeb.Error,
               response: ClientListGetResponse) => void
  ): grpcWeb.ClientReadableStream<ClientListGetResponse>;

  clientAdd(
    request: ClientAddRequest,
    metadata: grpcWeb.Metadata,
    callback: (err: grpcWeb.Error,
               response: ClientAddResponse) => void
  ): grpcWeb.ClientReadableStream<ClientAddResponse>;

  clientEdit(
    request: ClientEditRequest,
    metadata: grpcWeb.Metadata,
    callback: (err: grpcWeb.Error,
               response: ClientEditResponse) => void
  ): grpcWeb.ClientReadableStream<ClientEditResponse>;

  clientRemove(
    request: ClientRemoveRequest,
    metadata: grpcWeb.Metadata,
    callback: (err: grpcWeb.Error,
               response: ClientRemoveResponse) => void
  ): grpcWeb.ClientReadableStream<ClientRemoveResponse>;

}

export class CorporateServicePromiseClient {
  constructor (hostname: string,
               credentials: null | { [index: string]: string; },
               options: null | { [index: string]: string; });

  authenticate(
    request: AuthenticateRequest,
    metadata: grpcWeb.Metadata
  ): Promise<AuthenticateResponse>;

  whoAmI(
    request: WhoAmIRequest,
    metadata: grpcWeb.Metadata
  ): Promise<WhoAmIResponse>;

  keywordRegenerate(
    request: KeywordRegenerateRequest,
    metadata: grpcWeb.Metadata
  ): Promise<KeywordRegenerateResponse>;

  tariffListGet(
    request: TariffListGetRequest,
    metadata: grpcWeb.Metadata
  ): Promise<TariffListGetResponse>;

  orderListGet(
    request: OrderListGetRequest,
    metadata: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<OrderListGetResponse>;

  orderCancel(
    request: OrderCancelRequest,
    metadata: grpcWeb.Metadata
  ): grpcWeb.ClientReadableStream<OrderCancelResponse>;

  orderListSingleGet(
    request: OrderListSingleGetRequest,
    metadata: grpcWeb.Metadata
  ): Promise<OrderListSingleGetResponse>;

  orderAdd(
    request: OrderAddRequest,
    metadata: grpcWeb.Metadata
  ): Promise<OrderAddResponse>;

  clientListGet(
    request: ClientListGetRequest,
    metadata: grpcWeb.Metadata
  ): Promise<ClientListGetResponse>;

  clientAdd(
    request: ClientAddRequest,
    metadata: grpcWeb.Metadata
  ): Promise<ClientAddResponse>;

  clientEdit(
    request: ClientEditRequest,
    metadata: grpcWeb.Metadata
  ): Promise<ClientEditResponse>;

  clientRemove(
    request: ClientRemoveRequest,
    metadata: grpcWeb.Metadata
  ): Promise<ClientRemoveResponse>;

}

