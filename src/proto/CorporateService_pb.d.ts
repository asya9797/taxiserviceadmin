export class AuthenticateRequest {
  constructor ();
  getLogin(): string;
  setLogin(a: string): void;
  getPassword(): string;
  setPassword(a: string): void;
  toObject(): AuthenticateRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => AuthenticateRequest;
}

export namespace AuthenticateRequest {
  export type AsObject = {
    Login: string;
    Password: string;
  }
}

export class AuthenticateResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getToken(): string;
  setToken(a: string): void;
  getRegion(): string;
  setRegion(a: string): void;
  getGateway1(): string;
  setGateway1(a: string): void;
  getGateway2(): string;
  setGateway2(a: string): void;
  getCorporationsList(): Corporation[];
  setCorporationsList(a: Corporation[]): void;
  toObject(): AuthenticateResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => AuthenticateResponse;
}

export namespace AuthenticateResponse {
  export type AsObject = {
    Status: number;
    Token: string;
    Region: string;
    Gateway1: string;
    Gateway2: string;
    CorporationsList: Corporation[];
  }
}

export class Client {
  constructor ();
  getCorporation(): number;
  setCorporation(a: number): void;
  getId(): number;
  setId(a: number): void;
  getName(): string;
  setName(a: string): void;
  getIdcontact(): number;
  setIdcontact(a: number): void;
  getContact(): string;
  setContact(a: string): void;
  getFrom(): number;
  setFrom(a: number): void;
  toObject(): Client.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => Client;
}

export namespace Client {
  export type AsObject = {
    Corporation: number;
    Id: number;
    Name: string;
    Idcontact: number;
    Contact: string;
    From: number;
  }
}

export class ClientAddRequest {
  constructor ();
  getToken(): string;
  setToken(a: string): void;
  getName(): string;
  setName(a: string): void;
  getContact(): string;
  setContact(a: string): void;
  getCorporation(): number;
  setCorporation(a: number): void;
  toObject(): ClientAddRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => ClientAddRequest;
}

export namespace ClientAddRequest {
  export type AsObject = {
    Token: string;
    Name: string;
    Contact: string;
    Corporation: number;
  }
}

export class ClientAddResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getClient(): Client;
  setClient(a: Client): void;
  toObject(): ClientAddResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => ClientAddResponse;
}

export namespace ClientAddResponse {
  export type AsObject = {
    Status: number;
    Client: Client;
  }
}

export class ClientEditRequest {
  constructor ();
  getToken(): string;
  setToken(a: string): void;
  getName(): string;
  setName(a: string): void;
  getContact(): string;
  setContact(a: string): void;
  getId(): number;
  setId(a: number): void;
  toObject(): ClientEditRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => ClientEditRequest;
}

export namespace ClientEditRequest {
  export type AsObject = {
    Token: string;
    Name: string;
    Contact: string;
    Id: number;
  }
}

export class ClientEditResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getClient(): Client;
  setClient(a: Client): void;
  toObject(): ClientEditResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => ClientEditResponse;
}

export namespace ClientEditResponse {
  export type AsObject = {
    Status: number;
    Client: Client;
  }
}

export class ClientListGetRequest {
  constructor ();
  getToken(): string;
  setToken(a: string): void;
  getSpecificcorporation(): number;
  setSpecificcorporation(a: number): void;
  toObject(): ClientListGetRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => ClientListGetRequest;
}

export namespace ClientListGetRequest {
  export type AsObject = {
    Token: string;
    Specificcorporation: number;
  }
}

export class ClientListGetResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getClientsList(): Client[];
  setClientsList(a: Client[]): void;
  toObject(): ClientListGetResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => ClientListGetResponse;
}

export namespace ClientListGetResponse {
  export type AsObject = {
    Status: number;
    ClientsList: Client[];
  }
}

export class ClientRemoveRequest {
  constructor ();
  getToken(): string;
  setToken(a: string): void;
  getId(): number;
  setId(a: number): void;
  toObject(): ClientRemoveRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => ClientRemoveRequest;
}

export namespace ClientRemoveRequest {
  export type AsObject = {
    Token: string;
    Id: number;
  }
}

export class ClientRemoveResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getClient(): Client;
  setClient(a: Client): void;
  toObject(): ClientRemoveResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => ClientRemoveResponse;
}

export namespace ClientRemoveResponse {
  export type AsObject = {
    Status: number;
    Client: Client;
  }
}

export class Corporation {
  constructor ();
  getId(): number;
  setId(a: number): void;
  getTitleshort(): string;
  setTitleshort(a: string): void;
  getTitlefull(): string;
  setTitlefull(a: string): void;
  getKeyword(): string;
  setKeyword(a: string): void;
  getBalance(): number;
  setBalance(a: number): void;
  toObject(): Corporation.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => Corporation;
}

export namespace Corporation {
  export type AsObject = {
    Id: number;
    Titleshort: string;
    Titlefull: string;
    Keyword: string;
    Balance: number;
  }
}

export class KeywordRegenerateRequest {
  constructor ();
  getToken(): string;
  setToken(a: string): void;
  getCorporation(): number;
  setCorporation(a: number): void;
  toObject(): KeywordRegenerateRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => KeywordRegenerateRequest;
}

export namespace KeywordRegenerateRequest {
  export type AsObject = {
    Token: string;
    Corporation: number;
  }
}

export class KeywordRegenerateResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getCorporation(): Corporation;
  setCorporation(a: Corporation): void;
  toObject(): KeywordRegenerateResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => KeywordRegenerateResponse;
}

export namespace KeywordRegenerateResponse {
  export type AsObject = {
    Status: number;
    Corporation: Corporation;
  }
}

export class Order {
  constructor ();
  getCreatetime(): number;
  setCreatetime(a: number): void;
  getDeliverytime(): number;
  setDeliverytime(a: number): void;
  getTimeofarrival(): number;
  setTimeofarrival(a: number): void;
  getServernow(): number;
  setServernow(a: number): void;
  getId(): number;
  setId(a: number): void;
  getState(): number;
  setState(a: number): void;
  getCost(): number;
  setCost(a: number): void;
  getDistance(): number;
  setDistance(a: number): void;
  getIsscheduled(): number;
  setIsscheduled(a: number): void;
  getNumber(): number;
  setNumber(a: number): void;
  getFeedbackrating(): number;
  setFeedbackrating(a: number): void;
  getIdcategorygroup(): number;
  setIdcategorygroup(a: number): void;
  getIdcategory(): number;
  setIdcategory(a: number): void;
  getIdfamily(): number;
  setIdfamily(a: number): void;
  getIdcard(): number;
  setIdcard(a: number): void;
  getIdfavoritedriver(): number;
  setIdfavoritedriver(a: number): void;
  getIdcar(): number;
  setIdcar(a: number): void;
  getIddriver(): number;
  setIddriver(a: number): void;
  getIdclient(): number;
  setIdclient(a: number): void;
  getIdcorporate(): number;
  setIdcorporate(a: number): void;
  getIdservice(): number;
  setIdservice(a: number): void;
  getTipenabled(): number;
  setTipenabled(a: number): void;
  getCurrenttipvalue(): number;
  setCurrenttipvalue(a: number): void;
  getDeliverystr(): string;
  setDeliverystr(a: string): void;
  getEntrance(): string;
  setEntrance(a: string): void;
  getDestinationstr(): string;
  setDestinationstr(a: string): void;
  getFeedbacknotes(): string;
  setFeedbacknotes(a: string): void;
  getDescription(): string;
  setDescription(a: string): void;
  getDrivername(): string;
  setDrivername(a: string): void;
  getDrivertranslation(): string;
  setDrivertranslation(a: string): void;
  getCarnumber(): string;
  setCarnumber(a: string): void;
  getCarmodelname(): string;
  setCarmodelname(a: string): void;
  getCarmodeltranslation(): string;
  setCarmodeltranslation(a: string): void;
  getCarcolorname(): string;
  setCarcolorname(a: string): void;
  getCarcolorcode(): string;
  setCarcolorcode(a: string): void;
  getCarcolortranslation(): string;
  setCarcolortranslation(a: string): void;
  getPayer(): string;
  setPayer(a: string): void;
  getCurrenttiptype(): string;
  setCurrenttiptype(a: string): void;
  getCurrentcategoryicon(): string;
  setCurrentcategoryicon(a: string): void;
  getCurrentcategorytranslation(): string;
  setCurrentcategorytranslation(a: string): void;
  getCurrentcategorygroupiconpassive(): string;
  setCurrentcategorygroupiconpassive(a: string): void;
  getCurrentcategorygroupiconactive(): string;
  setCurrentcategorygroupiconactive(a: string): void;
  getCurrentcategorygrouptranslation(): string;
  setCurrentcategorygrouptranslation(a: string): void;
  getCurrentcorporationtitle(): string;
  setCurrentcorporationtitle(a: string): void;
  getCurrentpaymentcardnumber(): string;
  setCurrentpaymentcardnumber(a: string): void;
  getCurrentpaymentcardtype(): string;
  setCurrentpaymentcardtype(a: string): void;
  getCurrentpaymentcardaccountname(): string;
  setCurrentpaymentcardaccountname(a: string): void;
  getCurrentfamilypackagename(): string;
  setCurrentfamilypackagename(a: string): void;
  getDelivery(): string;
  setDelivery(a: string): void;
  getDestinations(): string;
  setDestinations(a: string): void;
  toObject(): Order.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => Order;
}

export namespace Order {
  export type AsObject = {
    Createtime: number;
    Deliverytime: number;
    Timeofarrival: number;
    Servernow: number;
    Id: number;
    State: number;
    Cost: number;
    Distance: number;
    Isscheduled: number;
    Number: number;
    Feedbackrating: number;
    Idcategorygroup: number;
    Idcategory: number;
    Idfamily: number;
    Idcard: number;
    Idfavoritedriver: number;
    Idcar: number;
    Iddriver: number;
    Idclient: number;
    Idcorporate: number;
    Idservice: number;
    Tipenabled: number;
    Currenttipvalue: number;
    Deliverystr: string;
    Entrance: string;
    Destinationstr: string;
    Feedbacknotes: string;
    Description: string;
    Drivername: string;
    Drivertranslation: string;
    Carnumber: string;
    Carmodelname: string;
    Carmodeltranslation: string;
    Carcolorname: string;
    Carcolorcode: string;
    Carcolortranslation: string;
    Payer: string;
    Currenttiptype: string;
    Currentcategoryicon: string;
    Currentcategorytranslation: string;
    Currentcategorygroupiconpassive: string;
    Currentcategorygroupiconactive: string;
    Currentcategorygrouptranslation: string;
    Currentcorporationtitle: string;
    Currentpaymentcardnumber: string;
    Currentpaymentcardtype: string;
    Currentpaymentcardaccountname: string;
    Currentfamilypackagename: string;
    Delivery: string;
    Destinations: string;
  }
}

export class OrderAddRequest {
  constructor ();
  getToken(): string;
  setToken(a: string): void;
  getDeliverytime(): number;
  setDeliverytime(a: number): void;
  getIdcategorygroup(): number;
  setIdcategorygroup(a: number): void;
  getIdcategory(): number;
  setIdcategory(a: number): void;
  getIdclient(): number;
  setIdclient(a: number): void;
  getIdcorporate(): number;
  setIdcorporate(a: number): void;
  getIdservice(): number;
  setIdservice(a: number): void;
  getTipenabled(): number;
  setTipenabled(a: number): void;
  getCurrenttipvalue(): number;
  setCurrenttipvalue(a: number): void;
  getCurrenttiptype(): string;
  setCurrenttiptype(a: string): void;
  getEntrance(): string;
  setEntrance(a: string): void;
  getDescription(): string;
  setDescription(a: string): void;
  getDelivery(): string;
  setDelivery(a: string): void;
  getDestinations(): string;
  setDestinations(a: string): void;
  toObject(): OrderAddRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => OrderAddRequest;
}

export namespace OrderAddRequest {
  export type AsObject = {
    Token: string;
    Deliverytime: number;
    Idcategorygroup: number;
    Idcategory: number;
    Idclient: number;
    Idcorporate: number;
    Idservice: number;
    Tipenabled: number;
    Currenttipvalue: number;
    Currenttiptype: string;
    Entrance: string;
    Description: string;
    Delivery: string;
    Destinations: string;
  }
}

export class OrderAddResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getOrder(): Order;
  setOrder(a: Order): void;
  toObject(): OrderAddResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => OrderAddResponse;
}

export namespace OrderAddResponse {
  export type AsObject = {
    Status: number;
    Order: Order;
  }
}

export class OrderCancelRequest {
  constructor ();
  getToken(): string;
  setToken(a: string): void;
  getId(): number;
  setId(a: number): void;
  toObject(): OrderCancelRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => OrderCancelRequest;
}

export namespace OrderCancelRequest {
  export type AsObject = {
    Token: string;
    Id: number;
  }
}

export class OrderCancelResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getOrder(): Order;
  setOrder(a: Order): void;
  toObject(): OrderCancelResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => OrderCancelResponse;
}

export namespace OrderCancelResponse {
  export type AsObject = {
    Status: number;
    Order: Order;
  }
}

export class OrderListGetRequest {
  constructor ();
  getToken(): string;
  setToken(a: string): void;
  getSpecificcorporation(): number;
  setSpecificcorporation(a: number): void;
  toObject(): OrderListGetRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => OrderListGetRequest;
}

export namespace OrderListGetRequest {
  export type AsObject = {
    Token: string;
    Specificcorporation: number;
  }
}

export class OrderListGetResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getOrder(): Order;
  setOrder(a: Order): void;
  toObject(): OrderListGetResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => OrderListGetResponse;
}

export namespace OrderListGetResponse {
  export type AsObject = {
    Status: number;
    Order: Order;
  }
}

export class OrderListSingleGetRequest {
  constructor ();
  getToken(): string;
  setToken(a: string): void;
  getSpecificcorporation(): number;
  setSpecificcorporation(a: number): void;
  toObject(): OrderListSingleGetRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => OrderListSingleGetRequest;
}

export namespace OrderListSingleGetRequest {
  export type AsObject = {
    Token: string;
    Specificcorporation: number;
  }
}

export class OrderListSingleGetResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getOrderList(): Order[];
  setOrderList(a: Order[]): void;
  toObject(): OrderListSingleGetResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => OrderListSingleGetResponse;
}

export namespace OrderListSingleGetResponse {
  export type AsObject = {
    Status: number;
    OrderList: Order[];
  }
}

export class Point {
  constructor ();
  getLat(): number;
  setLat(a: number): void;
  getLon(): number;
  setLon(a: number): void;
  toObject(): Point.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => Point;
}

export namespace Point {
  export type AsObject = {
    Lat: number;
    Lon: number;
  }
}

export class Tariff {
  constructor ();
  getCategoryid(): number;
  setCategoryid(a: number): void;
  getCategoryname(): string;
  setCategoryname(a: string): void;
  getCategorytranslation(): string;
  setCategorytranslation(a: string): void;
  getCategoryicon(): string;
  setCategoryicon(a: string): void;
  getCategoryorder(): number;
  setCategoryorder(a: number): void;
  getCategoryisdefault(): number;
  setCategoryisdefault(a: number): void;
  getServiceid(): number;
  setServiceid(a: number): void;
  getServicename(): string;
  setServicename(a: string): void;
  toObject(): Tariff.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => Tariff;
}

export namespace Tariff {
  export type AsObject = {
    Categoryid: number;
    Categoryname: string;
    Categorytranslation: string;
    Categoryicon: string;
    Categoryorder: number;
    Categoryisdefault: number;
    Serviceid: number;
    Servicename: string;
  }
}

export class TariffGroup {
  constructor ();
  getGroupid(): number;
  setGroupid(a: number): void;
  getGrouptranslation(): string;
  setGrouptranslation(a: string): void;
  getGroupiconactive(): string;
  setGroupiconactive(a: string): void;
  getGroupiconpassive(): string;
  setGroupiconpassive(a: string): void;
  getGrouporder(): number;
  setGrouporder(a: number): void;
  getGroupisdefault(): number;
  setGroupisdefault(a: number): void;
  getTariffsList(): Tariff[];
  setTariffsList(a: Tariff[]): void;
  toObject(): TariffGroup.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => TariffGroup;
}

export namespace TariffGroup {
  export type AsObject = {
    Groupid: number;
    Grouptranslation: string;
    Groupiconactive: string;
    Groupiconpassive: string;
    Grouporder: number;
    Groupisdefault: number;
    TariffsList: Tariff[];
  }
}

export class TariffListGetRequest {
  constructor ();
  getToken(): string;
  setToken(a: string): void;
  getCorporation(): number;
  setCorporation(a: number): void;
  getFrom(): Point;
  setFrom(a: Point): void;
  getToList(): Point[];
  setToList(a: Point[]): void;
  toObject(): TariffListGetRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => TariffListGetRequest;
}

export namespace TariffListGetRequest {
  export type AsObject = {
    Token: string;
    Corporation: number;
    From: Point;
    ToList: Point[];
  }
}

export class TariffListGetResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getCorporation(): number;
  setCorporation(a: number): void;
  getTariffgroupsList(): TariffGroup[];
  setTariffgroupsList(a: TariffGroup[]): void;
  toObject(): TariffListGetResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => TariffListGetResponse;
}

export namespace TariffListGetResponse {
  export type AsObject = {
    Status: number;
    Corporation: number;
    TariffgroupsList: TariffGroup[];
  }
}

export class WhoAmIRequest {
  constructor ();
  getToken(): string;
  setToken(a: string): void;
  toObject(): WhoAmIRequest.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => WhoAmIRequest;
}

export namespace WhoAmIRequest {
  export type AsObject = {
    Token: string;
  }
}

export class WhoAmIResponse {
  constructor ();
  getStatus(): number;
  setStatus(a: number): void;
  getCorporationsList(): Corporation[];
  setCorporationsList(a: Corporation[]): void;
  toObject(): WhoAmIResponse.AsObject;
  serializeBinary(): Uint8Array;
  static deserializeBinary: (bytes: {}) => WhoAmIResponse;
}

export namespace WhoAmIResponse {
  export type AsObject = {
    Status: number;
    CorporationsList: Corporation[];
  }
}

