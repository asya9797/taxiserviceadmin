/**
 * @fileoverview gRPC-Web generated client stub for router
 * @enhanceable
 * @public
 */

// GENERATED CODE -- DO NOT EDIT!



const grpc = {};
grpc.web = require('grpc-web');

const proto = {};
proto.router = require('./CorporateService_pb.js');

/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.router.CorporateServiceClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!grpc.web.GrpcWebClientBase} The client
   */
  this.client_ = new grpc.web.GrpcWebClientBase(options);

  /**
   * @private @const {string} The hostname
   */
  this.hostname_ = hostname;

  /**
   * @private @const {?Object} The credentials to be used to connect
   *    to the server
   */
  this.credentials_ = credentials;

  /**
   * @private @const {?Object} Options for the client
   */
  this.options_ = options;
};


/**
 * @param {string} hostname
 * @param {?Object} credentials
 * @param {?Object} options
 * @constructor
 * @struct
 * @final
 */
proto.router.CorporateServicePromiseClient =
    function(hostname, credentials, options) {
  if (!options) options = {};
  options['format'] = 'binary';

  /**
   * @private @const {!proto.router.CorporateServiceClient} The delegate callback based client
   */
  this.delegateClient_ = new proto.router.CorporateServiceClient(
      hostname, credentials, options);

};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.AuthenticateRequest,
 *   !proto.router.AuthenticateResponse>}
 */
const methodInfo_CorporateService_Authenticate = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.AuthenticateResponse,
  /** @param {!proto.router.AuthenticateRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.AuthenticateResponse.deserializeBinary
);


/**
 * @param {!proto.router.AuthenticateRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.router.AuthenticateResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.router.AuthenticateResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.authenticate =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/router.CorporateService/Authenticate',
      request,
      metadata,
      methodInfo_CorporateService_Authenticate,
      callback);
};


/**
 * @param {!proto.router.AuthenticateRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.router.AuthenticateResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.authenticate =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.authenticate(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.WhoAmIRequest,
 *   !proto.router.WhoAmIResponse>}
 */
const methodInfo_CorporateService_WhoAmI = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.WhoAmIResponse,
  /** @param {!proto.router.WhoAmIRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.WhoAmIResponse.deserializeBinary
);


/**
 * @param {!proto.router.WhoAmIRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.router.WhoAmIResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.router.WhoAmIResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.whoAmI =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/router.CorporateService/WhoAmI',
      request,
      metadata,
      methodInfo_CorporateService_WhoAmI,
      callback);
};


/**
 * @param {!proto.router.WhoAmIRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.router.WhoAmIResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.whoAmI =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.whoAmI(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.KeywordRegenerateRequest,
 *   !proto.router.KeywordRegenerateResponse>}
 */
const methodInfo_CorporateService_KeywordRegenerate = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.KeywordRegenerateResponse,
  /** @param {!proto.router.KeywordRegenerateRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.KeywordRegenerateResponse.deserializeBinary
);


/**
 * @param {!proto.router.KeywordRegenerateRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.router.KeywordRegenerateResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.router.KeywordRegenerateResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.keywordRegenerate =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/router.CorporateService/KeywordRegenerate',
      request,
      metadata,
      methodInfo_CorporateService_KeywordRegenerate,
      callback);
};


/**
 * @param {!proto.router.KeywordRegenerateRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.router.KeywordRegenerateResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.keywordRegenerate =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.keywordRegenerate(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.TariffListGetRequest,
 *   !proto.router.TariffListGetResponse>}
 */
const methodInfo_CorporateService_TariffListGet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.TariffListGetResponse,
  /** @param {!proto.router.TariffListGetRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.TariffListGetResponse.deserializeBinary
);


/**
 * @param {!proto.router.TariffListGetRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.router.TariffListGetResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.router.TariffListGetResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.tariffListGet =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/router.CorporateService/TariffListGet',
      request,
      metadata,
      methodInfo_CorporateService_TariffListGet,
      callback);
};


/**
 * @param {!proto.router.TariffListGetRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.router.TariffListGetResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.tariffListGet =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.tariffListGet(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.OrderListGetRequest,
 *   !proto.router.OrderListGetResponse>}
 */
const methodInfo_CorporateService_OrderListGet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.OrderListGetResponse,
  /** @param {!proto.router.OrderListGetRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.OrderListGetResponse.deserializeBinary
);


/**
 * @param {!proto.router.OrderListGetRequest} request The request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.router.OrderListGetResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.orderListGet =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/router.CorporateService/OrderListGet',
      request,
      metadata,
      methodInfo_CorporateService_OrderListGet);
};


/**
 * @param {!proto.router.OrderListGetRequest} request The request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.router.OrderListGetResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.orderListGet =
    function(request, metadata) {
  return this.delegateClient_.client_.serverStreaming(this.delegateClient_.hostname_ +
      '/router.CorporateService/OrderListGet',
      request,
      metadata,
      methodInfo_CorporateService_OrderListGet);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.OrderCancelRequest,
 *   !proto.router.OrderCancelResponse>}
 */
const methodInfo_CorporateService_OrderCancel = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.OrderCancelResponse,
  /** @param {!proto.router.OrderCancelRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.OrderCancelResponse.deserializeBinary
);


/**
 * @param {!proto.router.OrderCancelRequest} request The request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.router.OrderCancelResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.orderCancel =
    function(request, metadata) {
  return this.client_.serverStreaming(this.hostname_ +
      '/router.CorporateService/OrderCancel',
      request,
      metadata,
      methodInfo_CorporateService_OrderCancel);
};


/**
 * @param {!proto.router.OrderCancelRequest} request The request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!grpc.web.ClientReadableStream<!proto.router.OrderCancelResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.orderCancel =
    function(request, metadata) {
  return this.delegateClient_.client_.serverStreaming(this.delegateClient_.hostname_ +
      '/router.CorporateService/OrderCancel',
      request,
      metadata,
      methodInfo_CorporateService_OrderCancel);
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.OrderListSingleGetRequest,
 *   !proto.router.OrderListSingleGetResponse>}
 */
const methodInfo_CorporateService_OrderListSingleGet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.OrderListSingleGetResponse,
  /** @param {!proto.router.OrderListSingleGetRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.OrderListSingleGetResponse.deserializeBinary
);


/**
 * @param {!proto.router.OrderListSingleGetRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.router.OrderListSingleGetResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.router.OrderListSingleGetResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.orderListSingleGet =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/router.CorporateService/OrderListSingleGet',
      request,
      metadata,
      methodInfo_CorporateService_OrderListSingleGet,
      callback);
};


/**
 * @param {!proto.router.OrderListSingleGetRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.router.OrderListSingleGetResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.orderListSingleGet =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.orderListSingleGet(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.OrderAddRequest,
 *   !proto.router.OrderAddResponse>}
 */
const methodInfo_CorporateService_OrderAdd = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.OrderAddResponse,
  /** @param {!proto.router.OrderAddRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.OrderAddResponse.deserializeBinary
);


/**
 * @param {!proto.router.OrderAddRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.router.OrderAddResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.router.OrderAddResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.orderAdd =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/router.CorporateService/OrderAdd',
      request,
      metadata,
      methodInfo_CorporateService_OrderAdd,
      callback);
};


/**
 * @param {!proto.router.OrderAddRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.router.OrderAddResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.orderAdd =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.orderAdd(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.ClientListGetRequest,
 *   !proto.router.ClientListGetResponse>}
 */
const methodInfo_CorporateService_ClientListGet = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.ClientListGetResponse,
  /** @param {!proto.router.ClientListGetRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.ClientListGetResponse.deserializeBinary
);


/**
 * @param {!proto.router.ClientListGetRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.router.ClientListGetResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.router.ClientListGetResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.clientListGet =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/router.CorporateService/ClientListGet',
      request,
      metadata,
      methodInfo_CorporateService_ClientListGet,
      callback);
};


/**
 * @param {!proto.router.ClientListGetRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.router.ClientListGetResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.clientListGet =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.clientListGet(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.ClientAddRequest,
 *   !proto.router.ClientAddResponse>}
 */
const methodInfo_CorporateService_ClientAdd = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.ClientAddResponse,
  /** @param {!proto.router.ClientAddRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.ClientAddResponse.deserializeBinary
);


/**
 * @param {!proto.router.ClientAddRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.router.ClientAddResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.router.ClientAddResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.clientAdd =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/router.CorporateService/ClientAdd',
      request,
      metadata,
      methodInfo_CorporateService_ClientAdd,
      callback);
};


/**
 * @param {!proto.router.ClientAddRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.router.ClientAddResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.clientAdd =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.clientAdd(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.ClientEditRequest,
 *   !proto.router.ClientEditResponse>}
 */
const methodInfo_CorporateService_ClientEdit = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.ClientEditResponse,
  /** @param {!proto.router.ClientEditRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.ClientEditResponse.deserializeBinary
);


/**
 * @param {!proto.router.ClientEditRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.router.ClientEditResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.router.ClientEditResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.clientEdit =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/router.CorporateService/ClientEdit',
      request,
      metadata,
      methodInfo_CorporateService_ClientEdit,
      callback);
};


/**
 * @param {!proto.router.ClientEditRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.router.ClientEditResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.clientEdit =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.clientEdit(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


/**
 * @const
 * @type {!grpc.web.AbstractClientBase.MethodInfo<
 *   !proto.router.ClientRemoveRequest,
 *   !proto.router.ClientRemoveResponse>}
 */
const methodInfo_CorporateService_ClientRemove = new grpc.web.AbstractClientBase.MethodInfo(
  proto.router.ClientRemoveResponse,
  /** @param {!proto.router.ClientRemoveRequest} request */
  function(request) {
    return request.serializeBinary();
  },
  proto.router.ClientRemoveResponse.deserializeBinary
);


/**
 * @param {!proto.router.ClientRemoveRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @param {function(?grpc.web.Error, ?proto.router.ClientRemoveResponse)}
 *     callback The callback function(error, response)
 * @return {!grpc.web.ClientReadableStream<!proto.router.ClientRemoveResponse>|undefined}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServiceClient.prototype.clientRemove =
    function(request, metadata, callback) {
  return this.client_.rpcCall(this.hostname_ +
      '/router.CorporateService/ClientRemove',
      request,
      metadata,
      methodInfo_CorporateService_ClientRemove,
      callback);
};


/**
 * @param {!proto.router.ClientRemoveRequest} request The
 *     request proto
 * @param {!Object<string, string>} metadata User defined
 *     call metadata
 * @return {!Promise<!proto.router.ClientRemoveResponse>}
 *     The XHR Node Readable Stream
 */
proto.router.CorporateServicePromiseClient.prototype.clientRemove =
    function(request, metadata) {
  return new Promise((resolve, reject) => {
    this.delegateClient_.clientRemove(
      request, metadata, (error, response) => {
        error ? reject(error) : resolve(response);
      });
  });
};


module.exports = proto.router;

