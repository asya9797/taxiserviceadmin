import { Component, OnInit } from '@angular/core';
import { TranslateService } from './com/annaniks/taxiServiceAdmin/services/translate.service';
import { ActivatedRoute } from '../../node_modules/@angular/router';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  title = 'taxiServiceAdmin';
  
  ngOnInit() {}
}
