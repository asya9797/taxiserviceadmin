import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { SharedModule } from './com/annaniks/taxiServiceAdmin/shared/shared.module';
import { ApiService } from './com/annaniks/taxiServiceAdmin/services/api.service';
import { CookieService } from 'angular2-cookie/core';
import { AuthGuard } from './com/annaniks/taxiServiceAdmin/services/authGuard.service';
import { NguCarouselModule } from '@ngu/carousel';
import { TranslateService } from './com/annaniks/taxiServiceAdmin/services/translate.service';
import { HttpClientModule } from '../../node_modules/@angular/common/http';
@NgModule({
  declarations: [
    AppComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    BrowserAnimationsModule,
    SharedModule,
    NguCarouselModule
  ],
  providers: [
    {
      provide: 'BASE_URL', useValue: 'https://reactive.rocket.am:9091'
    },
    ApiService,
    CookieService,
    AuthGuard,
    TranslateService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
