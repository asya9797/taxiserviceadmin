import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { AuthGuard } from './com/annaniks/taxiServiceAdmin/services/authGuard.service';

const routes: Routes = [
  {
    path: 'main',
    loadChildren: 'src/app/com/annaniks/taxiServiceAdmin/views/main/main.module#MainModule', canActivate: [AuthGuard]
  },
  {
    path: "",
    loadChildren: "src/app/com/annaniks/taxiServiceAdmin/views/login/login.module#LoginModule"
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
