import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';




@Component({
    selector: 'add-message-to-driver',
    templateUrl: './add-message-to-driver.modal.html',
    styleUrls: ['./add-message-to-driver.modal.scss']
})
export class AddMessageToDriverModal implements OnInit {

    public messageToDriverForm: FormGroup;
    constructor(public messageDialogRef: MatDialogRef<AddMessageToDriverModal>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }
    ngOnInit() {
        this._buildForm()
    }

    onNoClick(): void {
        this.messageDialogRef.close();
    }


    private _buildForm(): void {
        this.messageToDriverForm = new FormBuilder().group({
            entrance: ['', Validators.required],
            description: ['', Validators.required],
        })
    }
    public clickToSave(){
        this.messageDialogRef.close({entrance: this.messageToDriverForm.value.entrance, description: this.messageToDriverForm.value.description});
    }


}