import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';




@Component({
    selector: 'leave-tips',
    templateUrl: './leave-tips.modal.html',
    styleUrls: ['./leave-tips.modal.scss']
})
export class LeaveTipsModal implements OnInit {
    public tipForm: FormGroup;
    public device;
    public percent: boolean = false;
    public amount : boolean = true;
    constructor(public dialogRef: MatDialogRef<LeaveTipsModal>,
        @Inject(MAT_DIALOG_DATA) public data: any) { }
    ngOnInit() {
       this._buildForm()
    //    this.checkDialogData()
      

    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    private _buildForm(): void {
        this.tipForm = new FormBuilder().group({
            percent: ['', Validators.required],
            amount: ['', Validators.required],
            active: [0, Validators.required],



        })
    }

    onChange(value) {
        if (value.checked === true) {
            this.tipForm.value.active = 1;
            this.percent = true;
            this.amount = false;
            console.log( this.tipForm.value.active);
        } else {
            this.tipForm.value.active = 0;
            this.percent = false;
            this.amount = true;
            console.log( this.tipForm.value.active);
        }
    }

    public clickSave(){
        if(this.percent && this.tipForm.value.percent){
            this.dialogRef.close({tip: this.tipForm.value.percent,tipType: "%"})
        }else if(this.amount && this.tipForm.value.amount){
            this.dialogRef.close({tip: this.tipForm.value.amount,tipType: "դր․"})
        }
       
    }
    // public checkDialogData():void{
    //     if(this.data.item){
    //         if(this.percent){
    //             this.tipForm.patchValue({
    //                 percent: this.data.item,
    //             })
    //         }else if(this.amount){
    //             this.tipForm.patchValue({
    //                 amount: this.data.item,
    //             })
    //         }   
    //        }else{
    //            if(this.percent){
    //             this.tipForm.patchValue({
    //                 percent: ''
    //             })
    //            }else if(this.amount){
    //             this.tipForm.patchValue({
    //                 amount: ''
    //             })
    //            }
           
    //        }
    // }
  

}