import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { DatePipe } from '@angular/common';


@Component({
    selector: 'delivery-time',
    templateUrl: './delivery-time.modal.html',
    styleUrls: ['./delivery-time.modal.scss']
})
export class DeliveryTimeModal implements OnInit {
    public today = new Date();
    public dateFormat: string;
    public dateWhenClickNow: string;
    public deliveryTimeForm: FormGroup;
    public timeAndDatePickers: boolean = true;
    constructor(public deliveryDialogRef: MatDialogRef<DeliveryTimeModal>,
        @Inject(MAT_DIALOG_DATA) public data: any, public datePipe: DatePipe) { }
    ngOnInit() {
        this._buildForm();
        this.dateWhenClickNow = this.today.getUTCDate() + '/' + (this.today.getUTCMonth() + 1 ) + '/' + this.today.getUTCFullYear();
        console.log(this.dateWhenClickNow);
    }

    onNoClick(): void {
        this.deliveryDialogRef.close();
    }


    private _buildForm(): void {
        this.deliveryTimeForm = new FormBuilder().group({
            active: [0, Validators.required],
            onlyTime: ['', Validators.required],
            date: ['',Validators.required],
            time: ['',Validators.required]

        })
    }

    public onChange(value): void {
        if (value.checked === true) {
            this.deliveryTimeForm.value.active = 1;
            this.timeAndDatePickers = false;
            console.log(this.deliveryTimeForm.value.active);
        } else {
            this.deliveryTimeForm.value.active = 0;
            this.timeAndDatePickers = true;
            console.log(this.deliveryTimeForm.value.active);
        }
    }
    public clickSave() {
        if (this.timeAndDatePickers && this.deliveryTimeForm.value.onlyTime) {
            this.deliveryDialogRef.close({ date: this.dateWhenClickNow, time: this.deliveryTimeForm.value.onlyTime, });
        } else if (!this.timeAndDatePickers && this.deliveryTimeForm.value.date && this.deliveryTimeForm.value.time) {
            this.dateFormat = this.datePipe.transform(new Date(this.deliveryTimeForm.value.date), 'dd/M/yyyy');
            this.deliveryDialogRef.close({ date:  this.dateFormat, time: this.deliveryTimeForm.value.time });
        }
    }



}