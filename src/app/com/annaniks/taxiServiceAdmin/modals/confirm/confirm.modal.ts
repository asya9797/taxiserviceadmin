import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
@Component({
    selector: 'confirm',
    templateUrl: './confirm.modal.html',
    styleUrls: ['./confirm.modal.scss']
})
export class ConfirmModal implements OnInit {
    constructor(public dialogRef: MatDialogRef<ConfirmModal>,
        @Inject(MAT_DIALOG_DATA) public data: any, ) { }
    ngOnInit() {
        console.log("confirm modal data",this.data)
     }

    public onClickYes(): void {
        this.dialogRef.close(true);
    }

    public onClickNo(): void {
        this.dialogRef.close(false)
    }
}