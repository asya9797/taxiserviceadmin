import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { ClientEditResponse } from '../../../../../../proto/CorporateService_pb';



@Component({
    selector: 'settings-of-client',
    templateUrl: './settings-of-client.modal.html',
    styleUrls: ['./settings-of-client.modal.scss']
})
export class SettingsOfClientModal implements OnInit {
    public editClientForm: FormGroup;
    public loading:boolean = false;
    constructor(public settingsDialogRef: MatDialogRef<SettingsOfClientModal>,
        @Inject(MAT_DIALOG_DATA) public data: any, private _apiService: ApiService) { }
    ngOnInit() {
        this._buildForm();
        this.seeOldInfoOfClient()
       
      
    }

    onNoClick(): void {
        this.settingsDialogRef.close();
    }

    private _buildForm(): void {
        this.editClientForm = new FormBuilder().group({
            name: ['', Validators.required],
            phoneNum: ['', Validators.required],
           
        })
    }

    public editEmployee() {
        this.loading = true;
        this.editClientForm.enable()
        this._apiService.testClientEditRequest(this.data.getId(),this.editClientForm.value.phoneNum, this.editClientForm.value.name).then((data: ClientEditResponse) => {
            console.log("edit client data", data);
            this.loading = false;
            this.editClientForm.disable()
            this.settingsDialogRef.close(true);
        })
    }

    public seeOldInfoOfClient(): void {
        if (this.data) {
            console.log(this.data);
            this.editClientForm.patchValue({
                name: this.data.getName(),
                phoneNum: this.data.getContact()
            })

        }
    }
  

}