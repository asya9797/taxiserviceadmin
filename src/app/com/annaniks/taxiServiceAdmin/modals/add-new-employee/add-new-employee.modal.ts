import { Component, OnInit, Inject } from '@angular/core';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ApiService } from '../../services/api.service';
import { ClientAddResponse, WhoAmIResponse } from '../../../../../../proto/CorporateService_pb';
import { ActivatedRoute } from '@angular/router';



@Component({
    selector: 'add-new-employee',
    templateUrl: './add-new-employee.modal.html',
    styleUrls: ['./add-new-employee.modal.scss']
})
export class AddNewEmployeeModal implements OnInit {
    public loading: boolean = false;
    public newEmployeeForm: FormGroup;
    public corporations;
    public corporationId: number;
   
    constructor(public dialogRef: MatDialogRef<AddNewEmployeeModal>,
        @Inject(MAT_DIALOG_DATA) public data: any, private _apiService: ApiService, private route: ActivatedRoute) {

            this.route.queryParams
            .subscribe(params => {
              if (params && params.idOfCorporation) {
                this.corporationId = +params.idOfCorporation;
              } else {
                this._apiService.testWhoAmI().then((data: WhoAmIResponse) => {
                  this.corporationId = data.getCorporationsList()[0].getId()
                  console.log(this.corporationId);
      
                }, (error) => {
      
                })
              }
            });
         }
    ngOnInit() {
        this._buildForm()
        this.getCorporations()
      
    }

    onNoClick(): void {
        this.dialogRef.close();
    }

    private _buildForm(): void {
        this.newEmployeeForm = new FormBuilder().group({
            name: ['', Validators.required],
            phoneNum: ['', Validators.required],
            
        })
    }

    public addNewEmployee() {
        this._apiService.testClientAddRequest(this.newEmployeeForm.value.phoneNum, this.newEmployeeForm.value.name, this.corporationId).then((data: ClientAddResponse) => {
            console.log("add client data", data);
            this.dialogRef.close(true);
        })
    }
    public getCorporations(){
        this._apiService.testWhoAmI().then((data: WhoAmIResponse) => {
            this.corporations = data.getCorporationsList()
        })
    }

}