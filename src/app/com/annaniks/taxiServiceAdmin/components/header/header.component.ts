import { Component, OnInit } from '@angular/core';
import { Language } from '../../models/models';
import { Router, ActivatedRoute } from '@angular/router';
import { CookieService } from 'angular2-cookie/core';
import { ApiService } from '../../services/api.service';
import { element } from '../../../../../../../node_modules/@angular/core/src/render3';

@Component({
    selector: 'header',
    templateUrl: './header.component.html',
    styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {

    public changeLng: boolean = false;
    public displaying: string;
    public disp: boolean = false;
    public currentLanguage: Language;
    languages: Language[] = [
        { title: 'Հայերեն', flag: 'assets/assets/arm.jpg', key: 'arm' },
        { title: 'English', flag: 'assets/assets/en.png', key: 'eng' },
        { title: 'Русский', flag: 'assets/assets/ru.png', key: 'ru' }
    ];
    constructor(private _router: Router, private _cookieService: CookieService, private _apiService: ApiService, private _activatedRoute: ActivatedRoute) {
        this.displaying = 'none';

    }
    ngOnInit() {
        let language: string = this._activatedRoute.snapshot.queryParams.language;
        let filteredLanguage = this.languages.filter((element) => element.key === language);
        if (filteredLanguage.length === 1) {
            this.currentLanguage = filteredLanguage[0];
        }
        else {
            this.currentLanguage = this.languages[0];
        }
    }

    public clickToChangeLanguage(): void {
        this.changeLng = !this.changeLng
    }
    public changeLanguage(item): void {
        console.log(item);
        this._router.navigate([], { relativeTo: this._activatedRoute, queryParams: { language: item.key } }).then(() => {
            window.location.reload();
        })


    }
    public toggleOpenedSidebar(): void {
        if (this.displaying === 'none') {
            this.displaying = 'list-item'
        } else if (this.displaying === 'list-item') {
            this.displaying = 'none'
        }
    }
    public clickLogOut(): void {
        this._cookieService.removeAll();
        this._router.navigate(["/login"])

    }

    public clickMenu() {
        this.disp = !this.disp
    }

}