
import { Component, OnInit, Input } from '@angular/core';
import { MenuItemsService } from '../../services/menuItems.service';
import { DatePipe } from '@angular/common';
import { ExcelService } from '../../services/excelService.service';
import { Order, OrderListSingleGetResponse, ClientListGetResponse } from '../../../../../../proto/CorporateService_pb';
import { PaginatorPageNumber } from '../../models/models';
import { ApiService } from '../../services/api.service';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';


@Component({
  selector: 'data-table',
  templateUrl: './data-table.component.html',
  styleUrls: ['./data-table.component.scss'],

})
export class DataTableComponent implements OnInit {
  @Input() queryParams;
  @Input() scheduled;
  @Input() isScheduled;
  public corporationId: number;
  public tableOfOrders = []
  public orderList: Order[];
  public rangeDates: Array<Date> = [];
  public forFilterData: boolean = false;
  public limit: number = 10;
  public count: number;
  public page: number = 1;
  public today = new Date();
  public allTableOfOrders = [];
  public searchForm: FormGroup;

  constructor(private router: Router, private _datePipe: DatePipe, private excelService: ExcelService, private _apiService: ApiService, private route: ActivatedRoute) {
    this.route.queryParams
      .subscribe(params => {
        if (params) {
          if (params.seeAll) {
            this.corporationId = -1;
            console.log(this.corporationId);
          } else if (params.idOfCorporation) {
            this.corporationId = +params.idOfCorporation;
            console.log(this.corporationId);
          }
        }
        this.ordersList(this.corporationId)

      });

    let start = (this.today.getUTCMonth() + 1) + '/' + '01/' + this.today.getUTCFullYear();
    let end = (this.today.getUTCMonth() + 1) + '/' + this.today.getUTCDate() + '/' + this.today.getUTCFullYear();
    this.rangeDates.push(new Date(start));
    this.rangeDates.push(new Date(end))

  }
  ngOnInit() {
    this._buildForm()
    this.ordersList(this.corporationId)
   
  }


  
  private _buildForm(): void {
    this.searchForm = new FormBuilder().group({

        searching: ['', Validators.required],


    })
    this.searchForm.get('searching').valueChanges.subscribe((data) => {
        this.itemSearch(data)
        console.log(data);

    })
}
  public filter(): void {
    this.forFilterData = true;
    if (this.rangeDates && this.rangeDates[0] && this.rangeDates[1]) {
      let startDate = this._datePipe.transform(this.rangeDates[0], 'dd/MM/yyyy');
      let endDate = this._datePipe.transform(this.rangeDates[1], 'dd/MM/yyyy');
    }
  }
  public ordersList(corpId) {
    this._apiService.testOrderListSingleGetRequest(corpId).then((data: OrderListSingleGetResponse) => {
      this.count = data.getOrderList().length;
      this.orderList = data.getOrderList().slice(this.limit * (this.page - 1), this.page * this.limit);
      this.allTableOfOrders = this.orderList.filter(item => {
        return item.getIsscheduled() == this.isScheduled
      })
      this.tableOfOrders = this.allTableOfOrders
      console.log( this.allTableOfOrders, "alll tablee naxqan fory");
      this._apiService.testClientListGetRequest(corpId).then((data: ClientListGetResponse) => {
        for(let i = 0 ; i< this.allTableOfOrders.length;i++){
         for(let j = 0; j< data.getClientsList().length;j++){ 
           if(this.allTableOfOrders[i].getIdclient() == data.getClientsList()[j].getId()){
             this.allTableOfOrders[i]["name"] = data.getClientsList()[j].getName();
             this.allTableOfOrders[i]["contact"] = data.getClientsList()[j].getContact();  
           }
         }
        }
      })  
    });
 
  }

  public itemSearch(item) {

    this.tableOfOrders = this.allTableOfOrders.filter(itm => {
      console.log(itm,"booo");
      console.log(itm.array[10])
      return ((itm.name.toUpperCase().search(item.toUpperCase()) != -1) || (itm.contact.toUpperCase().search(item.toUpperCase()) != -1) ||  (itm.array[24].toUpperCase().search(item.toUpperCase()) != -1))
    })
    if (item == "") {
        this.ordersList(this.corporationId)
    }
}
  deleteDate() {
    this.rangeDates = undefined;
    this.forFilterData = false;

  }

  exportAsXLSX(): void {
    this.excelService.exportAsExcelFile(this.tableOfOrders, 'sample');
  }

  public paginate(event: PaginatorPageNumber): void {
    console.log("eventy", event);
    this.page = event.pageNumber;
    this.ordersList(this.corporationId)
  }

  public addNewOrder(): void {
    this.router.navigate(['/main/newOrder'])
  }
}