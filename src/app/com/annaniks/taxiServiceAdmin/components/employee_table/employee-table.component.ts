import { Component, OnInit, Input, Output, EventEmitter } from "@angular/core";
import { MatDialog } from '@angular/material';
import { ActivatedRoute } from '@angular/router';
import { ApiService } from "../../services/api.service";
import { AddNewEmployeeModal } from "../../modals/add-new-employee/add-new-employee.modal";
import { ClientListGetResponse, ClientRemoveResponse, Client } from "../../../../../../proto/CorporateService_pb";
import { PaginatorPageNumber } from "../../models/models";
import { SettingsOfClientModal } from "../../modals/settings-of-client/settings-of-client.modal";
import { ConfirmModal } from "../../modals/confirm/confirm.modal";
import { FormGroup, FormBuilder, Validators } from "@angular/forms";
import { ExcelService } from "../../services/excelService.service";



@Component({
    selector: 'employee-table',
    templateUrl: './employee-table.component.html',
    styleUrls: ['./employee-table.component.scss']
})
export class EmployeeTableComponent implements OnInit {
    @Input() queryParams;
    @Input() from;
    public namesList = [];
    public tableOfClients = []
    public clientsTable;
    public allTableOfClients = [];
    public loading: boolean = false;
    public corporationId: number;
    public limit: number = 5;
    public page: number = 1;
    public count: number;
    public checkSortDescendingByName: boolean = false;
    public thereIsNothing: boolean = false;
    public searchForm: FormGroup;
    constructor(private dialog: MatDialog, private _apiService: ApiService, private route: ActivatedRoute,private excelService: ExcelService) {
        this.route.queryParams
            .subscribe(params => {
                if (params && params.idOfCorporation) {
                    this.corporationId = +params.idOfCorporation;
                    console.log(params);
                    this.getClients(this.corporationId)
                }

            });
    }
    ngOnInit() {
        this.getClients(this.corporationId);
        this._buildForm()
    }


    private _buildForm(): void {
        this.searchForm = new FormBuilder().group({

            searching: ['', Validators.required],


        })
        this.searchForm.get('searching').valueChanges.subscribe((data) => {
            this.itemSearch(data)
            console.log(data);

        })
    }


    public newEmployeeDialog(): void {
        const dialogRef = this.dialog.open(AddNewEmployeeModal, {
            width: '460px',


        })
        dialogRef.afterClosed().subscribe(result => {
            if (result) {
                this.getClients(this.corporationId)
            }
        });

    }
    public clickToSortingByName() {
        if (this.checkSortDescendingByName == true) {
            this.checkSortDescendingByName = false;
            this.tableOfClients = this.sortArray(this.tableOfClients, "descending")
        } else if (this.checkSortDescendingByName == false) {
            this.checkSortDescendingByName = true;
            this.tableOfClients = this.sortArray(this.tableOfClients, "ascending")
        }
    }

    public sortArray(array, sortingType) {

        if (sortingType == "descending") {
            array.sort((a, b) => {
                if (a.getName() < b.getName()) { return -1; }
                if (a.getName() > b.getName()) { return 1; }
                return 0;
            })
        } else {
            array.sort((a, b) => {
                if (a.getName() < b.getName()) { return 1; }
                if (a.getName() > b.getName()) { return -1; }
                return 0;
            })
        }
        console.log(array);
        return array




    }

    public getClients(corpId) {
        this.loading = true;
        this._apiService.testClientListGetRequest(corpId).then((data: ClientListGetResponse) => {
            console.log("clients list", data);
            this.count = data.getClientsList().length
            var array = data.getClientsList();
            if (this.checkSortDescendingByName == true) {
                array = this.sortArray(array, "ascending")
            } else {
                array = this.sortArray(array, "descending")
            }

            console.log(array);

            this.clientsTable = array.slice(this.limit * (this.page - 1), this.page * this.limit);
            if (this.from == 0) {
                this.allTableOfClients = this.clientsTable.filter(item => {
                    return item.getFrom() != this.from
                })
            } else {
                this.allTableOfClients = this.clientsTable.filter(item => {
                    return item.getFrom() == this.from
                })

            }
            this.tableOfClients = this.allTableOfClients
            console.log("tmp", this.tableOfClients);
            console.log(this.tableOfClients);
            this.loading = false;
        })
    }
    public itemSearch(item) {

        this.tableOfClients = this.allTableOfClients.filter(itm => {
            console.log(itm.getName(),"booo");
            console.log(item,"booo");
            console.log(itm.getContact(),"booo");
            
            
            
            return (itm.getName().toUpperCase().search(item.toUpperCase()) != -1 || itm.getContact().toUpperCase().search(item.toUpperCase()) != -1)
        })
        if (item == "") {
            this.getClients(this.corporationId)
        }
    }
    public removeClient(clientId) {
        this._apiService.testClientRemoveRequest(clientId).then((data: ClientRemoveResponse) => {
            console.log(data);
            this.getClients(this.corporationId)

        })
    }

    public paginate(event: PaginatorPageNumber): void {
        console.log("eventy", event);
        this.page = event.pageNumber;
        this.getClients(this.corporationId)
    }

    public editClient(item: Client): void {
        const settingsDialogRef = this.dialog.open(SettingsOfClientModal, {
            width: '500px',
            data: item
        })
        settingsDialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.getClients(this.corporationId)
            }
        })
    }
    public openConfirmModal(clientId): void {
        let dialogRef = this.dialog.open(ConfirmModal, {
            width: '380px'
        })
        dialogRef.afterClosed().subscribe((result) => {
            if (result) {
                this.removeClient(clientId);
                this.getClients(this.corporationId)

            }
        })
    }
    exportAsXLSX(): void {
        this.excelService.exportAsExcelFile(this.tableOfClients, 'sample');
      }
    


}