import { Component, OnInit, Output, EventEmitter } from '@angular/core';
import { animate, AUTO_STYLE, state, style, transition, trigger } from '@angular/animations';
import { MenuItemsService } from '../../services/menuItems.service';
import { ApiService } from '../../services/api.service';
import { Corporation, WhoAmIResponse, KeywordRegenerateResponse } from '../../../../../../proto/CorporateService_pb';
import { ActivatedRoute, Router } from '@angular/router';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'menu',
  templateUrl: './menu.component.html',
  styleUrls: ['./menu.component.scss']
})
export class MenuComponent implements OnInit {
  public displaying: boolean;
  public value: string;
  public corporationList;
  public numberOfCorporation: number;
  public id: number;
  public today = new Date();
  public index: number = 0;
  public nameOfOffice: string;
  public seeAllParam: boolean;
  public keyWord: string;
  public corporationId: number;
  public seeAllForm: FormGroup;
  public checkSeeAll: boolean;
  constructor(public menuItemsService: MenuItemsService, private _apiService: ApiService, private route: ActivatedRoute, private router: Router) {
    this.getCorporations()
    this.displaying = true;
    this.route.queryParams
      .subscribe(params => {
        if (params && params.idOfCorporation ) {
          this.corporationId = +params.idOfCorporation;
          
        } else {
          this._apiService.testWhoAmI().then((data: WhoAmIResponse) => {
            this.corporationId = data.getCorporationsList()[0].getId()
            console.log(this.corporationId);

          }, (error) => {

          })
        }

        if(params && params.seeAll){
          this.checkSeeAll = params.seeAll
        }
      });
      
  }

  ngOnInit() {
    this.getCorporations();
    this._buildForm()

    console.log("hii");

  }
  public _buildForm() {
    this.seeAllForm = new FormBuilder().group({
      seeAll: [false, Validators.required],
    
    });
 
      this.seeAllForm.get("seeAll").patchValue(this.checkSeeAll);
    
  }
  public chooseOffice(off) {
    this.nameOfOffice = off.getTitleshort();
    this.keyWord = off.getKeyword();
    this.value = off.getBalance();
    this.numberOfCorporation = off.getId();


  }

  public officeItem(off) {
    if (off.getTitleshort() === this.nameOfOffice) {
      return {
        'background': '#f0f0f0',
        'color': '#666',
        'border-radius': '5px',
        'box-shadow': ' 0 4px 4px 0 rgba(0,0,0,0.2)'
      }
    }

  }


  public onChange(value) {
    if (value.checked === true) {
      this.seeAllParam = true;
      this.displaying = false;
    } else {
      this.seeAllParam = false;
      this.displaying = true
    }
    this.router.navigate([],{relativeTo: this.route,queryParams:{seeAll: (this.seeAllParam)? true : null},queryParamsHandling: 'merge'})
  }

  public getCorporations() {
    this._apiService.testWhoAmI().then(((data: WhoAmIResponse) => {
      this.corporationList = data.getCorporationsList();
      this.nameOfOffice = this.corporationList[0].getTitleshort();
      this.keyWord = this.corporationList[0].getKeyword();
      this.value = this.corporationList[0].getBalance();
      //balance
      // console.log(this.corporationList[0].getBalance());
      this.numberOfCorporation = this.corporationList[0].getId();
      for (let index = 0; index < this.corporationList.length; index++) {
        if (this.corporationList[index].getId() == +this.id) {
          this.nameOfOffice = this.corporationList[index].getTitleshort();
          this.keyWord = this.corporationList[index].getKeyword();
          this.numberOfCorporation = this.corporationList[index].getId();
          this.value = this.corporationList[index].getBalance();
        }
      }
    }));
  }
  public clickToRegenerateKeyword(): void {
    this._apiService.testKeywordRegenerateRequest(this.corporationId).then((data: KeywordRegenerateResponse) => {
      console.log("keyword", data);
    })
  }


}
