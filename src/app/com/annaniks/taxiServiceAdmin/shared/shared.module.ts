import { NgModule } from '@angular/core';
import { HeaderComponent} from '../components/header/header.component';
import { CommonModule, DatePipe } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule,FormsModule } from '@angular/forms';
import { MaterialModule } from './material.module';
import { MenuComponent } from '../components/menu/menu.component';
import { DataTableComponent } from '../components/data-table/data-table.component';
import { CalendarModule } from "primeng/calendar";
import { PaginatorComponent } from '../components/paginator/paginator.component';
import { SlickCarouselModule } from 'ngx-slick-carousel';
import { OnlyNumber } from '../directives/onlyNumber.directive';
import {NgxMaterialTimepickerModule} from 'ngx-material-timepicker';
import { ConfirmModal } from '../modals/confirm/confirm.modal';
import { LoadingFullScreenComponent } from '../components/loading-full-screen/loading-full-screen.component';
import { LoadingComponent } from '../components/loading/loading.component';
import { EmployeeTableComponent } from '../components/employee_table/employee-table.component';
import { PipeTranslate } from '../services/translation.pipe';
import { ApiService } from '../services/api.service';


@NgModule({
    declarations: [PipeTranslate,LoadingComponent,OnlyNumber,HeaderComponent,MenuComponent,DataTableComponent,PaginatorComponent,LoadingFullScreenComponent,EmployeeTableComponent],
    imports: [CommonModule,RouterModule,ReactiveFormsModule,MaterialModule,CalendarModule,FormsModule,SlickCarouselModule,NgxMaterialTimepickerModule],
    exports: [PipeTranslate,LoadingComponent,LoadingFullScreenComponent,DatePipe,NgxMaterialTimepickerModule,OnlyNumber,HeaderComponent,CommonModule,MaterialModule,MenuComponent,DataTableComponent,CalendarModule,FormsModule,PaginatorComponent,SlickCarouselModule,EmployeeTableComponent],
    entryComponents: [],
    providers: [DatePipe]
})
export class SharedModule { }
