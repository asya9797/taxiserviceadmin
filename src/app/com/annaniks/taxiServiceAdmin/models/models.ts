export interface MenuItem {
  label: string;
  routerLink: string;
  icon: string;
  color: string;
  queryParams?: string;
  key: string
}
export interface Language {
  title: string;
  flag: string;
  key: string
}
export interface PaginatorPageNumber {
  pageNumber: number
}