import {Component, OnInit, Inject, ViewChild, NgZone} from '@angular/core';
import {ApiService} from '../../../services/api.service';
import {
  TariffListGetResponse,
  OrderAddResponse,
  OrderListSingleGetResponse,
  ClientListGetResponse
} from '../../../../../../../proto/CorporateService_pb';
import {ActivatedRoute} from '@angular/router';
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {SlickCarouselComponent} from 'ngx-slick-carousel';
import {LeaveTipsModal} from '../../../modals/leave-tips/leave-tips.modal';
import {MatDialog} from '@angular/material';
import {AddMessageToDriverModal} from '../../../modals/add-message-to-driver/add-message-to-driver.modal';
import {DeliveryTimeModal} from '../../../modals/delivery-time/delivery-time.modal';
import {ConfirmModal} from '../../../modals/confirm/confirm.modal';
import {debounceTime, map, startWith, switchMap} from 'rxjs/operators';
import {from, of} from 'rxjs';
import {flatMap} from 'rxjs/internal/operators';
import {promise} from 'selenium-webdriver';
import filter = promise.filter;

declare var google: any;
declare var ymaps: any;

@Component({
  selector: 'new-order',
  templateUrl: './new-order.component.html',
  styleUrls: ['./new-order.component.scss']
})
export class NewOrderComponent implements OnInit {
  public tariffSlides = [];
  public tariffSlideConfig = {
    slidesToShow: 3,
    slidesToScroll: 1,
    rows: 1,
    centerMode: true,
    focusOnSelect: true,
    arrows: true,
  };
  public hiding: boolean = false;
  public tipValue: string;
  public tariffGroups;
  public orderItemsForm: FormGroup;
  public corporationId = 1;
  public nameOfTipType: string;
  public finallyDeliveryValue: string;
  public check: boolean = false;
  public items: object[] = [];
  // public carouselBannerItems: Array<any> = [];
  // public carouselBanner: NgxCarousel;
  public currentCategoryId: number = 0;
  public addressFrom: any = {name: ''};
  public addressTo: any = {name: ''};
  public addressRef: any = 'from';
  public addressesAutoCompleteValues = [];
  public clientValues = [];
  public clientAutoCompleteValues = [];
  private moscowMap: any;
  private _lastRoute: any;
  private _employeeId: number;

  constructor(@Inject('BASE_URL') public baseUrl, private _apiService: ApiService, private _route: ActivatedRoute, private dialog: MatDialog, private _ngZone: NgZone) {
    this._route.queryParams
      .subscribe(params => {
        if (params && params.idOfCorporation) {
          this.corporationId = +params.idOfCorporation;
        }
        this._apiService
          .testClientListGetRequest(this.corporationId)
          .then(value => {
            if (value.getStatus() !== 200) {
              return;
            }
            for (let i = 0; i < value.getClientsList().length; i++) {
              this.clientValues.push({
                id: value.getClientsList()[i].getId(),
                name: value.getClientsList()[i].getName() !== value.getClientsList()[i].getContact() ?
                  value.getClientsList()[i].getName() + ', ' + value.getClientsList()[i].getContact() :
                  value.getClientsList()[i].getName()
              });
            }
          });
      });
      
  }

  ngOnInit() {
    this.orderItemsForm = new FormBuilder().group({
      deliveryTime: ['', Validators.required],
      idCategoryGroup: ['', Validators.required],
      idCategory: ['', Validators.required],
      idClient: ['', Validators.required],
      idCorporate: ['', Validators.required],
      idService: ['', Validators.required],
      tipEnabled: ['', Validators.required],
      checkboxActive: [0, Validators.required],
      forEdit: [this.tipValue, Validators.required],
      currentTipValue: ['', Validators.required],
      currentTipType: ['', Validators.required],
      delivery: ['', Validators.required],
      destination: ['', Validators.required],
      clientContact: ['', Validators.required]
    });
    this._initMap();

    this.orderItemsForm.get('delivery').valueChanges.pipe(
      flatMap(val => {
        return from(ymaps.search('Москва, ' + val));
      }),
      map((data: any) => {
        const addresses = [];
        for (let i = 0; i < data.geoObjects.getLength(); i++) {
          const geoObject = data.geoObjects.get(i);
          const addressObject = {
            name: geoObject.properties.get('address'),
            lat: geoObject.geometry.getCoordinates()[0],
            lon: geoObject.geometry.getCoordinates()[1]
          };
          addresses.push(addressObject);
        }
        return addresses;
      })
    ).subscribe(addresses => {
      this._ngZone.run(() => {
        this.addressesAutoCompleteValues = addresses;
      });
    });
    this.orderItemsForm.get('destination').valueChanges.pipe(
      flatMap(val => {
        return from(ymaps.search('Москва, ' + val));
      }),
      map((data: any) => {
        const addresses = [];
        for (let i = 0; i < data.geoObjects.getLength(); i++) {
          const geoObject = data.geoObjects.get(i);
          const addressObject = {
            name: geoObject.properties.get('address'),
            lat: geoObject.geometry.getCoordinates()[0],
            lon: geoObject.geometry.getCoordinates()[1]
          };
          addresses.push(addressObject);
        }
        return addresses;
      })
    ).subscribe(addresses => {
      this._ngZone.run(() => {
        this.addressesAutoCompleteValues = addresses;
      });
    });
    this.orderItemsForm.get('clientContact').valueChanges.pipe(
      map(val => {
        return this.clientValues.filter(client => {
          return client.name.toLowerCase().indexOf(val.toLowerCase()) !== -1;
        });
      })
    ).subscribe(clients => {
      this._ngZone.run(() => {
        this.clientAutoCompleteValues = clients;
      });
    });
  }

  onChange(value) {
    if (value.checked === true) {
      this.orderItemsForm.value.checkboxActive = 1;
      if (!this.tipValue) {
        this.leaveTipsDialog();
      } else {
        this.hiding = true;
      }

    } else {
      this.orderItemsForm.value.checkboxActive = 0;
      this.hiding = false;

    }
  }

  private getAddressRef() {
    return 'from' === this.addressRef ? this.addressFrom : this.addressTo;
  }

  private updateIconAndTariffs(addressLine, lat, lon) {
    this.getAddressRef().name = addressLine;
    this.getAddressRef().lat = lat;
    this.getAddressRef().lon = lon;

    this.updateTariffs();
    this.getAddressRef().placemark.properties.set({
      iconCaption: addressLine,
      balloonContent: false
    });
    if ('from' === this.addressRef) {
      this._ngZone.run(() => {
        this.orderItemsForm.get('delivery').setValue(addressLine);
        this.addressRef = 'to';
      });
    } else {
      this._ngZone.run(() => {
        this.orderItemsForm.get('destination').setValue(addressLine);
        this.addressRef = 'none';
      });
    }
  }

  private _initMap(): void {
    const thisRef = this;

    ymaps.ready(function () {
      thisRef.moscowMap = new ymaps.Map('yandex_map', {
        center: [55.76, 37.64],
        zoom: 16,
        controls: ['geolocationControl']
      }, {
        searchControlProvider: 'yandex#search'
      });
      thisRef.moscowMap.events.add('click', function (e) {
        const coords = e.get('coords');
        if (thisRef.addressRef === 'none') {
          return;
        }
        thisRef.putPlacemarkIfNeeded(coords);
        thisRef.getAddress(coords);
      });
    });
  }


  private putPlacemarkIfNeeded(coords) {
    if (this.getAddressRef().placemark) {
      this.getAddressRef().placemark.geometry.setCoordinates(coords);
    } else {
      this.getAddressRef().placemark = this.createPlacemark(coords);
      this.moscowMap.geoObjects.add(this.getAddressRef().placemark);
      const thisRef = this;
      this.getAddressRef().placemark.events.add('dragend', function () {
        thisRef.getAddress(thisRef.getAddressRef().placemark.geometry.getCoordinates());
      });
    }
  }

  private createPlacemark(coords) {
    return new ymaps.Placemark(coords, {
      iconCaption: 'Поиск аддресса ...',
      iconContent: this.addressRef === 'to' ? 'B' : 'A'
    }, {
      preset: this.addressRef === 'to' ? 'islands#orangeIcon' : 'islands#orangeIcon',
      draggable: true
    });
  }

  private getAddress(coords) {
    this.getAddressRef().placemark.properties.set('iconCaption', 'Поиск аддресса ...');
    const thisRef = this;
    ymaps.geocode(coords).then(function (res) {
      const firstGeoObject = res.geoObjects.get(0);
      thisRef.updateIconAndTariffs(
        firstGeoObject.getThoroughfare() && firstGeoObject.getPremiseNumber()
          ? firstGeoObject.getThoroughfare() + ' ' + firstGeoObject.getPremiseNumber()
          : firstGeoObject.getThoroughfare(),
        firstGeoObject.geometry.getCoordinates()[0],
        firstGeoObject.geometry.getCoordinates()[1]
      );
    });
  }

  public leaveTipsDialog(): void {
    const dialogRef = this.dialog.open(LeaveTipsModal, {
      width: '400px',
    });
    dialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result,"resultyy");
        this.tipValue = result.tip;
        this.orderItemsForm.patchValue({forEdit: result.tip});
        this.nameOfTipType = result.tipType;
        this.hiding = true;
      }else{
        this.orderItemsForm.patchValue({checkboxActive: 0});
      }
    });

  }

  public addMessageToDriverDialog(): void {
    const messageDialogRef = this.dialog.open(AddMessageToDriverModal, {
      width: '400px'
    });
    messageDialogRef.afterClosed().subscribe(result => {
      if (result) {
        console.log(result.entrance);
        console.log(result.description);
      }
    });

  }

  public deliveryTimeDialog(): void {
    const deliveryDialogRef = this.dialog.open(DeliveryTimeModal, {
      width: '400px',
    });
    deliveryDialogRef.afterClosed().subscribe(result => {
      if (result) {
        this.finallyDeliveryValue = result.date + ' ' + result.time;
        console.log(this.finallyDeliveryValue);
      }
    });

  }

  public clickDeleteTip(): void {
    if (this.tipValue) {
      this.tipValue = '';
      this.hiding = false;
      this.orderItemsForm.patchValue({checkboxActive: 0});
    }
  }


  public addOrder() {
    this._apiService.testOrderAddRequest(
      this.orderItemsForm.value.deliveryTime,
      this.orderItemsForm.value.idCategoryGroup,
      this.orderItemsForm.value.idCategory,
      this._employeeId,
      this.orderItemsForm.value.idCorporate,
      this.orderItemsForm.value.idService,
      this.orderItemsForm.value.tipEnabled,
      this.orderItemsForm.value.currentTipValue,
      this.orderItemsForm.value.currentTipType,
      this.orderItemsForm.value.entrance,
      this.orderItemsForm.value.description,
      this.addressFrom.name ? this.addressFrom.name + ';' + this.addressFrom.lat + ';' + this.addressFrom.lon + ';&' : null,
      this.addressTo.name ? this.addressTo.name + ';' + this.addressTo.lat + ';' + this.addressTo.lon + ';&' : null
    ).then((data: OrderAddResponse) => {
    }, (err) => {
    });
  }


  public updateTariffs() {
    this._updateTariffs(this.addressFrom, this.addressTo);
    if (this._lastRoute != null) {
      try {
        this.moscowMap.geoObjects.remove(this._lastRoute);
      } catch (e) {
        console.log(e);
      }
    }
    if (NewOrderComponent._validatePoint(this.addressFrom) && NewOrderComponent._validatePoint(this.addressTo)) {
      const thisRef = this;
      ymaps.route([
        {type: 'wayPoint', point: [this.addressFrom.lat, this.addressFrom.lon]},
        {type: 'wayPoint', point: [this.addressTo.lat, this.addressTo.lon]},
      ], {
        mapStateAutoApply: true,
      }).then(function (route) {
        thisRef._lastRoute = route;

        route.getPaths().options.set({
          strokeColor: '0000ffff',
          opacity: 0.9,
        });
        thisRef.moscowMap.geoObjects.add(route);
        route.getWayPoints().options.set('visible', false);
      });
    }
  }

  private _updateTariffs(pointFrom, pointTo) {
    const hasValidFrom = NewOrderComponent._validatePoint(pointFrom);
    const hasValidTo = NewOrderComponent._validatePoint(pointTo);
    if (hasValidFrom) {
      this._apiService.testTariffsGetRequest(
        this.corporationId,
        pointFrom.lat,
        pointFrom.lon,
        hasValidTo ? pointTo.lat : null,
        hasValidTo ? pointTo.lon : null)
        .then((data: TariffListGetResponse) => {
          this._ngZone.run(() => {
            this._updateTariffsContinuation(data);
          });
        }).catch(err => {
      });
    } else {
      this._ngZone.run(() => {
        this._updateTariffsContinuation(new TariffListGetResponse());
      });
    }
  }

  private static _validatePoint(point) {
    return point && point.lat && Math.abs(point.lat) > 0 && Math.abs(point.lat) && Math.abs(point.lon) > 0 && Math.abs(point.lon) > 0;
  }

  private _updateTariffsContinuation(data: TariffListGetResponse) {
    this.tariffGroups = data.getTariffgroupsList();
    this.tariffSlides.length = 0;
    for (let i = 0; i < data.getTariffgroupsList().length; i++) {
      for (let j = 0; j < data.getTariffgroupsList()[i].getTariffsList().length; j++) {
        let translationTitle = '';
        let translation = '';
        try {
          let translationObj = JSON.parse(data.getTariffgroupsList()[i].getTariffsList()[j].getCategorytranslation());
          for (let translationIndex = 0; translationIndex < translationObj.ru.length; translationIndex++) {
            if (translationIndex == 0) {
              translationTitle += translationObj.ru[translationIndex].k + ' ' + translationObj.ru[translationIndex].v;
            } else {
              translation += translationObj.ru[translationIndex].k + ' ' + translationObj.ru[translationIndex].v;
            }
          }
        } catch (e) {
          translationTitle = data.getTariffgroupsList()[i].getTariffsList()[j].getCategoryname();
          translation = data.getTariffgroupsList()[i].getTariffsList()[j].getCategoryname();
        }
        this.tariffSlides.push({
          id: data.getTariffgroupsList()[i].getTariffsList()[j].getCategoryid(),
          name: translationTitle,
          translation: translation,
          img: 'http://94.159.2.62' + data.getTariffgroupsList()[i].getTariffsList()[j].getCategoryicon(),
        });
      }
    }
    this.tariffSlideConfig.slidesToShow = this.tariffSlides.length > 3 ? 3 : this.tariffSlides.length;
  }

  public addressSelectedFromAutoComplete(e, field) {
    for (let i = 0; i < this.addressesAutoCompleteValues.length; i++) {
      if (e.option.value === this.addressesAutoCompleteValues[i].name) {
        const addressRefBefore = this.addressRef;
        this.addressRef = field;
        this.putPlacemarkIfNeeded([this.addressesAutoCompleteValues[i].lat, this.addressesAutoCompleteValues[i].lon]);
        this.getAddressRef().placemark.properties.set('iconCaption', 'Поиск аддресса ...');
        this.updateIconAndTariffs(
          this.addressesAutoCompleteValues[i].name,
          this.addressesAutoCompleteValues[i].lat,
          this.addressesAutoCompleteValues[i].lon
        );
        setTimeout(() => {
          this.addressRef = addressRefBefore;
        }, 1000);
      }
    }
  }

  public clientContactSelectedFromAutoComplete(e) {
    for (let i = 0; i < this.clientAutoCompleteValues.length; i++) {
      if (e.option.value === this.clientAutoCompleteValues[i].name) {
        this._employeeId = this.clientAutoCompleteValues[i].id;
      }
    }
  }

  public slickInit(e) {
  }

  public afterChange(e) {
    console.log(e);
  }

  public beforeChange(e) {
    console.log(e);
  }
}
