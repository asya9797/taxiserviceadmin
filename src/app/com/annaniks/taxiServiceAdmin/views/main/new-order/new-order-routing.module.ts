import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { NewOrderComponent } from "./new-order.component";
const routesNewOrder: Routes = [
    {
        path:"",
        component: NewOrderComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(routesNewOrder)],
    exports:[RouterModule]
})
export class NewOrderRoutingModule{}