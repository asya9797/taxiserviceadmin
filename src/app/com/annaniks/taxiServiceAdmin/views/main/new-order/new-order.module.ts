import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule, ReactiveFormsModule} from '@angular/forms';
import {NewOrderService} from './new-order.service';
import {NewOrderRoutingModule} from './new-order-routing.module';
import {NewOrderComponent} from './new-order.component';
import {SharedModule} from '../../../shared/shared.module';
import {MaterialModule} from '../../../shared/material.module';
import {LeaveTipsModal} from '../../../modals/leave-tips/leave-tips.modal';
import {AddMessageToDriverModal} from '../../../modals/add-message-to-driver/add-message-to-driver.modal';
import {DeliveryTimeModal} from '../../../modals/delivery-time/delivery-time.modal';
import {MatAutocompleteModule} from '@angular/material';

@NgModule({
  declarations: [NewOrderComponent, LeaveTipsModal, AddMessageToDriverModal, DeliveryTimeModal],
  imports: [CommonModule, NewOrderRoutingModule, SharedModule, MaterialModule, FormsModule, ReactiveFormsModule, MatAutocompleteModule],
  exports: [],
  providers: [NewOrderService],
  entryComponents: [LeaveTipsModal, AddMessageToDriverModal, DeliveryTimeModal]

})
export class NewOrderModule {
}
