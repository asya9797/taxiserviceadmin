import { Component, OnInit, ViewChild } from "@angular/core";
import { DatatableComponent } from '@swimlane/ngx-datatable';
import { DatePipe } from '@angular/common';
import { ApiService } from "../../../services/api.service";
import { OrderListSingleGetResponse, OrderListGetResponse, Order, WhoAmIResponse } from "../../../../../../../proto/CorporateService_pb";
import { ActivatedRoute } from "@angular/router";
ApiService

@Component({
  selector: 'orders',
  templateUrl: './orders.component.html',
  styleUrls: ['./orders.component.scss']
})
export class OrdersComponent implements OnInit {
  public orderList: Order[];
  public corporationId: number;
  public forAnimation: boolean = false
  public current: number = 0;
  public planned: number = 1;

  constructor(private _apiService: ApiService, private route: ActivatedRoute) {
    setTimeout( ()=> {
      this.forAnimation = true
    }, 200);
   
    this.route.queryParams
      .subscribe(params => {
        if (params && params.idOfCorporation) {
          this.corporationId = +params.idOfCorporation;
        } else {
          this._apiService.testWhoAmI().then((data: WhoAmIResponse) => {
            this.corporationId = data.getCorporationsList()[0].getId()
            console.log(this.corporationId);

          }, (error) => {

          })
        }
      });
  }
  ngOnInit() {

  }


}