import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { OrdersComponent } from "./orders.component";
const routesOrders: Routes = [
    {
        path:"",
        component: OrdersComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(routesOrders)],
    exports:[RouterModule]
})
export class OrdersRoutingModule{}