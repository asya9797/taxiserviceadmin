import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { OrdersService } from "./orders.service";
import { OrdersRoutingModule } from "./orders-routing.module";
import { OrdersComponent } from "./orders.component";
import { SharedModule} from "../../../shared/shared.module";
import { ExcelService } from "../../../services/excelService.service";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";



@NgModule({
    declarations:[OrdersComponent],
    imports:[CommonModule,OrdersRoutingModule,SharedModule,FormsModule,ReactiveFormsModule],
    exports:[],
    providers:[OrdersService,DatePipe,ExcelService]
})
export class OrdersModule{}