import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { MainComponent } from "./main.component";
const routesMain: Routes = [
    {
        path: "",
        component: MainComponent,
        children: [
            {
                path: 'newOrder',
                loadChildren: "src/app/com/annaniks/taxiServiceAdmin/views/main/new-order/new-order.module#NewOrderModule"
            },
            {
                path: '',
                redirectTo:'newOrder',
                pathMatch:'full'
                // loadChildren: "src/app/com/annaniks/taxiServiceAdmin/views/main/new-order/new-order.module#NewOrderModule"
            },
            {
                path: 'orders',
                loadChildren: "src/app/com/annaniks/taxiServiceAdmin/views/main/orders/orders.module#OrdersModule"
            },
            {
                path: 'employees',
                loadChildren: "src/app/com/annaniks/taxiServiceAdmin/views/main/employees/employees.module#EmployeesModule"
            },
        ]
    }
]
@NgModule({
    imports: [RouterModule.forChild(routesMain)],
    exports: [RouterModule]
})
export class MainRoutingModule { }