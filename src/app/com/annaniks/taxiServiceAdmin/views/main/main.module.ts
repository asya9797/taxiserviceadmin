import { NgModule } from "@angular/core";
import { CommonModule } from "@angular/common";
import { FormsModule, ReactiveFormsModule } from "@angular/forms";
import { MainService } from "./main.service";
import { MainRoutingModule } from "./main-routing.module";
import { MainComponent } from "./main.component";
import { SharedModule } from "../../shared/shared.module";
import { MenuItemsService } from "../../services/menuItems.service";


@NgModule({
    declarations: [MainComponent],
    imports: [CommonModule, FormsModule, ReactiveFormsModule, MainRoutingModule,SharedModule],
    exports: [],
    providers: [MainService,MenuItemsService]
})
export class MainModule {

}