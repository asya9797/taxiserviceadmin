import { NgModule } from "@angular/core";
import { Routes,RouterModule } from "@angular/router";
import { EmployeesComponent } from "./employees.component";
const routesEmployees: Routes = [
    {
        path:"",
        component: EmployeesComponent
    }
]
@NgModule({
    imports:[RouterModule.forChild(routesEmployees)],
    exports:[RouterModule]
})
export class EmployeesRoutingModule{}