import { Component, OnInit } from "@angular/core";
import { MatDialog } from '@angular/material';
import { AddNewEmployeeModal } from "../../../modals/add-new-employee/add-new-employee.modal";
import { ClientListGetResponse, Client, ClientRemoveResponse, WhoAmIResponse } from "../../../../../../../proto/CorporateService_pb";
import { ApiService } from "../../../services/api.service";
import { PaginatorPageNumber } from "../../../models/models";
import { ActivatedRoute } from '@angular/router';
import { SettingsOfClientModal } from "../../../modals/settings-of-client/settings-of-client.modal";
import { ConfirmModal } from "../../../modals/confirm/confirm.modal";

@Component({
    selector: 'employees',
    templateUrl: './employees.component.html',
    styleUrls: ['./employees.component.scss']
})
export class EmployeesComponent implements OnInit {
 
    public corporationId: number;
    public forAnimation : boolean = false;
    public bool: boolean;
    constructor(private dialog: MatDialog, private _apiService: ApiService, private route: ActivatedRoute) {
        setTimeout( ()=> {
          this.forAnimation = true
        }, 200);
       
        this.route.queryParams
            .subscribe(params => {
                
                if (params && params.idOfCorporation) {
                   
                    this.corporationId = +params.idOfCorporation; 
                } else {
                    this._apiService.testWhoAmI().then((data: WhoAmIResponse) => {
                     
                        this.corporationId = data.getCorporationsList()[0].getId()
                        console.log(this.corporationId);

                    }, (error) => {

                    })
                }

            });
           
    }
    ngOnInit() {}
    public checkEmployees(event): void {
        console.log(event);
        this.bool = event;

    }


   

}