import { NgModule } from "@angular/core";
import { CommonModule, DatePipe } from "@angular/common";
import { EmployeesService } from "./employees.service";
import { EmployeesRoutingModule } from "./employees-routing.module";
import { EmployeesComponent } from "./employees.component";
import { SharedModule} from "../../../shared/shared.module";
import { AddNewEmployeeModal } from "../../../modals/add-new-employee/add-new-employee.modal";
import { ReactiveFormsModule, FormsModule } from "@angular/forms";
import { SettingsOfClientModal } from "../../../modals/settings-of-client/settings-of-client.modal";
import { ConfirmModal } from "../../../modals/confirm/confirm.modal";
import { ExcelService } from "../../../services/excelService.service";
@NgModule({
    declarations:[EmployeesComponent,AddNewEmployeeModal,SettingsOfClientModal,ConfirmModal],
    imports:[CommonModule,EmployeesRoutingModule,SharedModule,FormsModule,ReactiveFormsModule],
    exports:[],
    providers:[EmployeesService,ExcelService],
    entryComponents:[AddNewEmployeeModal,SettingsOfClientModal,ConfirmModal]
})
export class EmployeesModule{}