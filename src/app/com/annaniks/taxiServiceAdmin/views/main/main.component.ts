import { Component, OnInit } from "@angular/core";
import { MenuItemsService } from "../../services/menuItems.service";
import { ApiService } from "../../services/api.service";
import { WhoAmIResponse, AuthenticateResponse, WhoAmIRequest } from "../../../../../../proto/CorporateService_pb";
import { CookieService } from "angular2-cookie/core";
import { ActivatedRoute } from "@angular/router";
import { TranslateService } from "../../services/translate.service";


@Component({
  selector: 'main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {
  public corporationsList;
  public corporationId: number;
  public isShow: boolean = false;
  constructor(private _apiService: ApiService, private _cookieService: CookieService, private route: ActivatedRoute, private _translateService: TranslateService) {
    let language: string = this.route.snapshot.queryParams.language;
    this._translateService.getTranslates((language) ? language : 'arm').subscribe((data) => {
      this.isShow = true;
    });
    this.route.queryParams
      .subscribe(params => {
        if (params && params.idOfCorporation) {
          this.corporationId = +params.idOfCorporation;
        } else {
          this._apiService.testWhoAmI().then((data: WhoAmIResponse) => {
            this.corporationId = data.getCorporationsList()[0].getId()
            console.log(this.corporationId);

          }, (error) => {

          })
        }
      });
  }

  ngOnInit() {
    console.log('ffff');


  }

}