import {Component, OnInit} from "@angular/core";
import {ApiService} from "../../services/api.service";
import {CookieService} from "angular2-cookie/core";
import {NavigationEnd, Router} from "@angular/router";

@Component({
  selector: 'landing',
  templateUrl: './landing.component.html',
  styleUrls: [
    './assets/css/bootstrap-grid.css',
    './assets/css/font-awesome.css',
    './assets/css/swiper.css',
    './assets/css/swipebox.css',
    './assets/css/zoomslider.css',
    './assets/css/style.css',
    './landing.component.scss'
  ]
})
export class LandingComponent implements OnInit {

  constructor(private _apiService: ApiService, private _cookieService: CookieService, private _router: Router) {
  }

  ngOnInit() {
    this._router.events.subscribe(s => {
      if (s instanceof NavigationEnd) {
        const tree = this._router.parseUrl(this._router.url);
        if (tree.fragment) {
          const element = document.querySelector("#" + tree.fragment);
          if (element) { element.scrollIntoView(true); }
        }
      }
    });
  }

  public  clickLogin():void{
    this._cookieService.removeAll();
    this._router.navigate(["/login"])
  }
}
