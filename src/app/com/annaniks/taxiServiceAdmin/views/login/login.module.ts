import {NgModule} from "@angular/core";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";
import {LoginRoutingModule} from "./login-routing.module";
import {LoginComponent} from "./login.component";
import {ApiService} from "../../services/api.service";
import {SharedModule} from "../../shared/shared.module";
import {MaterialModule} from "../../shared/material.module";
import {LandingComponent} from "./landing.component";


@NgModule({
  declarations: [LoginComponent, LandingComponent],
  imports: [CommonModule, FormsModule, ReactiveFormsModule, LoginRoutingModule, SharedModule, MaterialModule],
  exports: [],
  providers: [ApiService]
})
export class LoginModule {

}
