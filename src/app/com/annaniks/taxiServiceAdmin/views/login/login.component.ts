import {Component, OnInit} from "@angular/core";
import {FormGroup, FormBuilder, Validators} from '@angular/forms';
import {ApiService} from "../../services/api.service";
import {AuthenticateResponse} from "../../../../../../proto/CorporateService_pb";
import {CookieService} from "angular2-cookie/core";
import {Router} from "@angular/router";

@Component({
  selector: 'login',
  templateUrl: './login.component.html',
  styleUrls: [
    './assets/css/bootstrap-grid.css',
    './assets/css/font-awesome.css',
    './assets/css/swiper.css',
    './assets/css/swipebox.css',
    './assets/css/zoomslider.css',
    './assets/css/style.css',
    './landing.component.scss',
    './login.component.scss'
  ]
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;
  public errorMessage: string;

  constructor(private _apiService: ApiService, private _cookieService: CookieService, private _router: Router) {
  }

  ngOnInit() {
    this._buildForm()
  }

  private _buildForm() {
    this.loginForm = new FormBuilder().group({
      login: ['', Validators.required],
      password: ['', Validators.required],

    })
  }

  public clickSignIn() {
    this._apiService.authenticateRequest(this.loginForm.value.login, this.loginForm.value.password).then((data: AuthenticateResponse) => {
      let token = data.getToken();
      if (token) {
        this._cookieService.put('token', token);
        this._router.navigate(["/main"])
      } else {
        this.errorMessage = "Не правильный логин или пароль."

      }
    }, (error) => {
      console.log("errorr");
    })
  }

  public  clickLogin():void{
    this._cookieService.removeAll();
    this._router.navigate(["/login"])
  }
}
