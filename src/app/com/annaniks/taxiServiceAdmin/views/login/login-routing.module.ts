import {NgModule} from "@angular/core";
import {Routes, RouterModule} from "@angular/router";
import {LoginComponent} from "./login.component";
import {LandingComponent} from "./landing.component";


const routesLogin: Routes = [
  {
    path: "login",
    component: LoginComponent
  },
  {
    path: "",
    component: LandingComponent
  }
];

@NgModule({
  imports: [RouterModule.forChild(routesLogin)],
  exports: [RouterModule]
})
export class LoginRoutingModule {
}
