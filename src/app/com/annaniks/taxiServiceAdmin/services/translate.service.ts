import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { map } from '../../../../../../node_modules/rxjs/operators';


@Injectable()
export class TranslateService {
    public translate: object = {}
    public currentLanguage: string = '';
    constructor(private _httpClient: HttpClient) { }

    getTranslates(language: string) {
        let url: string = "/assets/translates/armenian.json"
        switch (language) {
            case 'eng': {
                console.log('fff');
                url = "/assets/translates/english.json";
                break;
            }
            case 'ru': {
                console.log('ruuu');
                url = "/assets/translates/russian.json";
                break;
            }
            case 'arm': {
                url = "/assets/translates/armenian.json";
                break;
            }
        }
        return this._httpClient.get(url).pipe(
            map((data) => {
                console.log(data);
                this.translate = data;
            })
        )
    }
}