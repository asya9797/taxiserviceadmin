
import { Pipe, PipeTransform } from "@angular/core";
import { ApiService } from './api.service';
import { TranslateService } from "./translate.service";

@Pipe({
    name: "translate"
})

export class PipeTranslate implements PipeTransform {

    constructor(private _translateService: TranslateService) { }

    transform(key: string) {
        return this._translateService.translate[key];
    }

}