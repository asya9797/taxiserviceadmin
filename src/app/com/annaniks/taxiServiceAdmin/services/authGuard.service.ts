import { Injectable } from "@angular/core";
import { CanActivate, Router } from '@angular/router';
import { Observable } from "rxjs";
import { CookieService } from "angular2-cookie/core";

@Injectable()
export class AuthGuard implements CanActivate {
    
    constructor(private _cookieService: CookieService,private _router:Router) { }

    canActivate(): Observable<boolean> | Promise<boolean> | boolean {
        if(this._cookieService.get('token')){
            return true
        }else{
            this._router.navigate(["/login"])
            return false
        }
    }
}