import { Injectable, Inject } from '@angular/core';
import { CorporateServiceClient, CorporateServicePromiseClient } from "../../../../../proto/CorporateService_grpc_web_pb";
import {
  AuthenticateRequest, ClientAddRequest, ClientEditRequest,
  ClientListGetRequest,
  OrderListGetRequest, TariffGroup, Tariff, TariffListGetRequest,
  WhoAmIRequest, OrderListSingleGetRequest, WhoAmIResponse, OrderAddRequest, Point, ClientRemoveResponse, ClientRemoveRequest, KeywordRegenerateResponse, KeywordRegenerateRequest
} from "../../../../../proto/CorporateService_pb";
import { CookieService } from 'angular2-cookie/core';
import { of, Observable } from 'rxjs';
import { ActivatedRoute } from '@angular/router';
import { TranslateService } from './translate.service';

const corporateServiceClient = new CorporateServiceClient("https://reactive.rocket.am:9080", { 'content-type': 'application/grpc+web' }, {});


//Authenticate
// const authenticateRequest = new AuthenticateRequest();
// authenticateRequest.setLogin("admin");
// authenticateRequest.setPassword("admin");


// corporateServiceClient.authenticate(authenticateRequest, { 'content-type': 'application/grpc+web' }, function (err, res) {
//   console.log(err);
//   console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//   console.log("corporateServiceClient.authenticate");
//   console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//   console.log(res.getToken());

//   let token = res.getToken();

//   let tariffRequest = new TariffListGetRequest();
//   tariffRequest.setLon(15);
//   tariffRequest.setLat(15);
//   tariffRequest.setToken(token);
//   tariffRequest.setCorporation(1);

//   corporateServiceClient.tariffListGet(tariffRequest, { 'content-type': 'application/grpc+web' }, function (err, response) {
//     if (err || response.getStatus() != 200) {
//       console.log(err);
//       return;
//     }
//     for (let i = 0; i < response.getTariffgroupsList().length; i++) {
//       let tariffGroup = response.getTariffgroupsList()[i];
//       console.log(tariffGroup.getGroupiconactive());
//     }
//   });


// });

// function doAfterTokenCalls(token: string) {
//   // testWhoAmIRequest(token);
//   // testClientAddRequest(token);
//   // testClientListGetRequest(token);
//   // testOrderListGetRequest(token);
//   // testTariffsGetRequest(token, 1);
//   // testOrderListSingeGetRequest(token);
// }

// function testWhoAmIRequest(token: string) {
//   // WhoAmI
//   const whoAmIRequest = new WhoAmIRequest();
//   whoAmIRequest.setToken(token);
//   corporateServiceClient.whoAmI(whoAmIRequest, { 'content-type': 'application/grpc+proto' }, function (err, res) {
//     console.log(err);
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log("corporateServiceClient.whoAmI");
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     for (let i = 0; i < res.getCorporationsList().length; i++) {
//       let corporation = res.getCorporationsList()[i];
//       console.log(corporation.getId());
//       console.log(corporation.getTitleshort());
//       console.log(corporation.getTitlefull());
//       console.log(corporation.getKeyword());
//     }
//   });
// }

// function testOrderListSingeGetRequest(token: string) {
//   // OrderListSingeGet
//   const orderListSingleGetRequest = new OrderListSingleGetRequest();
//   orderListSingleGetRequest.setToken(token);
//   corporateServiceClient.orderListSingleGet(orderListSingleGetRequest, { 'content-type': 'application/grpc+proto' }, function (err, res) {
//     console.log(err);
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log("corporateServiceClient.OrderListSingleGet");
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     for (let i = 0; i < res.getOrderList().length; i++) {
//       let order = res.getOrderList()[i];
//       console.log(order.getId());
//       console.log(order.getNumber());
//       console.log(order.getIdcorporate());
//     }
//   });
// }

// function testClientListGetRequest(token: string) {
//   // ClientListGet
//   const clientListGetRequest = new ClientListGetRequest();
//   clientListGetRequest.setToken(token);
//   corporateServiceClient.clientListGet(clientListGetRequest, { 'content-type': 'application/grpc+proto' }, function (err, res) {
//     console.log(err);
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log("corporateServiceClient.clientListGet");
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     for (let i = 0; i < res.getClientsList().length; i++) {
//       let client = res.getClientsList()[i];
//       console.log(client.getId());
//       console.log(client.getName());
//       console.log(client.getIdcontact());
//       console.log(client.getContact());
//       console.log(client.getCorporation());
//       console.log(client.getFrom());
//     }
//   });
// }

// function testClientAddRequest(token: string) {
//   // ClientAdd
//   const clientAddRequest = new ClientAddRequest();
//   clientAddRequest.setToken(token);
//   clientAddRequest.setContact("374966864xx");
//   clientAddRequest.setName("zz");
//   clientAddRequest.setCorporation(1);
//   corporateServiceClient.clientAdd(clientAddRequest, { 'content-type': 'application/grpc+proto' }, function (err, res) {
//     console.log(err);
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log("corporateServiceClient.clientAdd");
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     let clientEdited = res.getClient();
//     console.log(clientEdited.getId());
//     console.log(clientEdited.getName());
//     console.log(clientEdited.getIdcontact());
//     console.log(clientEdited.getContact());
//     console.log(clientEdited.getCorporation());
//     console.log(clientEdited.getFrom());
//     testClientEditRequest(token, clientEdited.getId());
//   });
// }

// function testClientEditRequest(token: string, id: number) {
//   // ClientEdit
//   const clientEditRequest = new ClientEditRequest();
//   clientEditRequest.setToken(token);
//   clientEditRequest.setId(id);
//   clientEditRequest.setContact("374966864xx");
//   clientEditRequest.setName("zzz");
//   corporateServiceClient.clientEdit(clientEditRequest, { 'content-type': 'application/grpc+proto' }, function (err, res) {
//     console.log(err);
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log("corporateServiceClient.clientEdit");
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     let clientEdited = res.getClient();
//     console.log(clientEdited.getId());
//     console.log(clientEdited.getName());
//     console.log(clientEdited.getIdcontact());
//     console.log(clientEdited.getContact());
//     console.log(clientEdited.getCorporation());
//     console.log(clientEdited.getFrom());
//   });
// }


// function testTariffsGetRequest(token: string, corporation: number) {
//   // ClientEdit
//   const tariffListGetRequest = new TariffListGetRequest();
//   tariffListGetRequest.setToken(token);
//   tariffListGetRequest.setCorporation(corporation);
//   tariffListGetRequest.setLat(10);
//   tariffListGetRequest.setLon(10);
//   corporateServiceClient.tariffListGet(tariffListGetRequest, { 'content-type': 'application/grpc+proto' }, function (err, res) {
//     console.log(err);
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log("corporateServiceClient.tariffListGet");
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     for (let i = 0; i < res.getTariffgroupsList().length; i++) {
//       let tariffGroup = res.getTariffgroupsList()[i];
//       console.log(tariffGroup.getGroupid());
//       console.log(tariffGroup.getGrouptranslation());
//       console.log(tariffGroup.getGroupiconactive());
//       console.log(tariffGroup.getGroupiconpassive());
//       console.log(tariffGroup.getGroupisdefault());
//       console.log(tariffGroup.getGrouporder());
//       for (let j = 0; j < tariffGroup.getTariffsList().length; j++) {
//         let tariff = tariffGroup.getTariffsList()[i];
//         console.log("\t\t\t" + tariff.getCategoryid());
//         console.log("\t\t\t" + tariff.getCategoryname());
//         console.log("\t\t\t" + tariff.getCategorytranslation());
//         console.log("\t\t\t" + tariff.getCategoryisdefault());
//         console.log("\t\t\t" + tariff.getCategoryorder());
//         console.log("\t\t\t" + tariff.getCategoryicon());
//         console.log("\n");
//       }
//       console.log("\n");
//     }
//   });
// }


// function testOrderListGetRequest(token: string) {
//   // orderListGet
//   const orderListGetRequest = new OrderListGetRequest();
//   orderListGetRequest.setToken(token);
//   const orderListGetRequestCall = corporateServiceClient.orderListGet(orderListGetRequest, { 'content-type': 'application/grpc+proto' });
//   orderListGetRequestCall.on("data", function (data) {
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log("corporateServiceClient.orderListGet");
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log(data.getOrder().getNumber());
//   });
//   orderListGetRequestCall.on("status", function (status) {
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log("corporateServiceClient.orderListGet");
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log(status);
//   });
//   orderListGetRequestCall.on("error", function (error) {
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log("corporateServiceClient.orderListGet");
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log(error);
//   });
//   orderListGetRequestCall.on("end", function () {
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log("corporateServiceClient.orderListGet");
//     console.log(">>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>");
//     console.log();
//   });
// }
@Injectable()
export class ApiService {
  private _corporateServicePromiseClient;



  constructor(@Inject('BASE_URL') private _baseUrl: string, private _cookieService: CookieService, ) {
    this._corporateServicePromiseClient = new CorporateServicePromiseClient(this._baseUrl, { 'content-type': 'application/grpc+web' }, {});

  



  }


  public authenticateRequest(login: string, password: string) {
    const authenticateRequest = new AuthenticateRequest();
    authenticateRequest.setLogin(login);
    authenticateRequest.setPassword(password);


    return this._corporateServicePromiseClient.authenticate(authenticateRequest, { 'content-type': 'application/grpc+web' }, (err, res) => {
      console.log(err);

      let token = res.getToken();
      this._cookieService.put('token', token);


    })

  }


  public testWhoAmI(): Promise<WhoAmIResponse> {
    const whoAmIRequest = new WhoAmIRequest();
    whoAmIRequest.setToken(this._cookieService.get('token') || '');
    return this._corporateServicePromiseClient.whoAmI(whoAmIRequest, { 'content-type': 'application/grpc+proto' });
  }

  public testOrderListSingleGetRequest(corporation) {
    console.log('fffff')
    const orderListSingleGetRequest = new OrderListSingleGetRequest();
    orderListSingleGetRequest.setToken(this._cookieService.get('token') || '');
    orderListSingleGetRequest.setSpecificcorporation(corporation);
    return this._corporateServicePromiseClient.orderListSingleGet(orderListSingleGetRequest, { 'content-type': 'application/grpc+proto' })
  }


  public testClientListGetRequest(corporation: number) {
    const clientListGetRequest = new ClientListGetRequest();
    clientListGetRequest.setToken(this._cookieService.get('token') || '');
    clientListGetRequest.setSpecificcorporation(corporation);
    return this._corporateServicePromiseClient.clientListGet(clientListGetRequest, { 'content-type': 'application/grpc+proto' });
  }

  public testClientAddRequest(phoneNum: string, name: string, corporation: number) {

    const clientAddRequest = new ClientAddRequest();
    clientAddRequest.setToken(this._cookieService.get('token') || '');
    clientAddRequest.setContact(phoneNum);
    clientAddRequest.setName(name);
    clientAddRequest.setCorporation(corporation);
    return this._corporateServicePromiseClient.clientAdd(clientAddRequest, { 'content-type': 'application/grpc+proto' })
  }

  public testClientEditRequest(id: number, contact: string, name: string) {
    // ClientEdit
    const clientEditRequest = new ClientEditRequest();
    clientEditRequest.setToken(this._cookieService.get('token') || '');
    clientEditRequest.setId(id);
    clientEditRequest.setContact(contact);
    clientEditRequest.setName(name);
    return this._corporateServicePromiseClient.clientEdit(clientEditRequest, { 'content-type': 'application/grpc+proto' });
  }

  public testTariffsGetRequest(corporation: number, latFrom: number, lonFrom: number, latTo, lonTo) {
    // ClientEdit
    const tariffListGetRequest = new TariffListGetRequest();
    tariffListGetRequest.setToken(this._cookieService.get('token') || '');
    tariffListGetRequest.setCorporation(corporation);
    const fromPoint = new Point();
    fromPoint.setLat(latFrom);
    fromPoint.setLon(lonFrom);
    tariffListGetRequest.setFrom(fromPoint);
    if (latTo && Math.abs(latTo) > 0 && lonTo && Math.abs(lonTo) > 0) {
      const toPoint = new Point();
      toPoint.setLat(latTo);
      toPoint.setLon(lonTo);
      tariffListGetRequest.setToList([toPoint]);
    }
    return this._corporateServicePromiseClient.tariffListGet(tariffListGetRequest, { 'content-type': 'application/grpc+proto' });
  }


  public testOrderListGetRequest(corporation: number) {
    // orderListGet
    const orderListGetRequest = new OrderListGetRequest();
    orderListGetRequest.setToken(this._cookieService.get('token') || '');
    orderListGetRequest.setSpecificcorporation(corporation);
    return this._corporateServicePromiseClient.orderListGet(orderListGetRequest, { 'content-type': 'application/grpc+proto' });

  }
  public testOrderAddRequest(deliveryTime: number, idCategoryGroup: number, idCategory: number, idClient: number, idCorporate: number, idService: number, tipEnabled: number, currentTipValue: number, currentTipType: string, entrance: string, description: string, delivery: string, destinations: string) {


    const orderAddRequest = new OrderAddRequest();
    orderAddRequest.setToken(this._cookieService.get('token') || '');
    orderAddRequest.setDeliverytime(deliveryTime);
    orderAddRequest.setIdcategorygroup(idCategoryGroup);
    orderAddRequest.setIdcategory(idCategory);
    orderAddRequest.setIdclient(idClient);
    orderAddRequest.setIdcorporate(idCorporate);
    orderAddRequest.setIdservice(idService);
    orderAddRequest.setTipenabled(tipEnabled);
    orderAddRequest.setCurrenttipvalue(currentTipValue)
    orderAddRequest.setCurrenttiptype(currentTipType),
      orderAddRequest.setEntrance(entrance)
    orderAddRequest.setDescription(description)
    orderAddRequest.setDelivery(delivery)
    orderAddRequest.setDestinations(destinations)
    return this._corporateServicePromiseClient.orderAdd(orderAddRequest, { 'content-type': 'application/grpc+proto' });
  }

  public testClientRemoveRequest(clientId): Promise<ClientRemoveResponse> {
    const clientRemoveRequest = new ClientRemoveRequest()
    clientRemoveRequest.setToken(this._cookieService.get('token') || '');
    clientRemoveRequest.setId(clientId)
    return this._corporateServicePromiseClient.clientRemove(clientRemoveRequest, { 'content-type': 'application/grpc+proto' });
  }

  public testKeywordRegenerateRequest(corporationId): Promise<KeywordRegenerateResponse> {
    const keywordRegenerateRequest = new KeywordRegenerateRequest()
    keywordRegenerateRequest.setToken(this._cookieService.get('token') || '');
    keywordRegenerateRequest.setCorporation(corporationId)
    return this._corporateServicePromiseClient.keywordRegenerate(keywordRegenerateRequest, { 'content-type': 'application/grpc+proto' });
  }




 




}