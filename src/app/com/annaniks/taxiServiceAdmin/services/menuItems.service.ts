import { Injectable } from '@angular/core';
import { MenuItem } from '../models/models';

@Injectable()
export class MenuItemsService {
    private _menuItems: Array<MenuItem> = [
        {
            label: "New order",
            routerLink: "/main/newOrder",
            icon: "feather icon-plus-square",
            color: "#219888",
            key: "newOrder"
        },
        {
            label: "Orders",
            routerLink: "/main/orders",
            icon: "feather icon-list",
            color: "#3386af",
            key: "orders"
        },
        {
            label: "Employees",
            routerLink: "/main/employees",
            icon: "feather icon-users",
            color: "#f48227",
            key:"employees"
        }
       
    ]

    constructor() { }

    public getMenuItems():Array<MenuItem>{
       return this._menuItems;
    }

}