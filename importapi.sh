#!/usr/bin/env bash
rm src/proto/*.js
rm src/proto/*.ts
protoc \
    --js_out=import_style=commonjs:./ \
    --grpc-web_out=import_style=commonjs+dts,mode=grpcweb:./ \
    ./src/proto/CorporateService.proto
